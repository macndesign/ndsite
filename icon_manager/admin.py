from django.contrib import admin
from icon_manager.models import Cor, Icone

class IconeAdmin(admin.ModelAdmin):
    list_display = ('icone_thumb', 'nome', 'tamanho', 'cor', 'imagem', 'ativo', 'inverso')
    list_editable = ('cor',)

admin.site.register(Cor)
admin.site.register(Icone, IconeAdmin)
