# coding: utf-8
from django import template

register = template.Library()

@register.filter(name="render_icones")
def render_icones(value):
    u"""
    Responsável por renderizar os ícones:
    Ex: {% autoescape off %}{{ normal_32|render_icones }}{% endautoescape %}
    """

    html = '<ul class="list-icons">'
    counter = 0

    for i in value:
        html += '<li><a class="mc-sprite-%(tamanho)s mc%(inverso)s-%(nome)s-%(tamanho)s mc-cor-%(cor)s" title="%(nome)s" href="javascript:void(0);">%(nome)s</a></li>'\
        % {'tamanho': i.tamanho, 'inverso': '-inverso' if i.inverso else '', 'nome': i.nome, 'cor': i.cor.nome}

        counter += 1

        if not counter % 5:
            html += '</ul><ul class="list-icons">'

    html += '</ul>'

    return html
