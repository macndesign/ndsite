# Create your views here.
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.list import ListView
from icon_manager.models import Icone, Cor

class IconeListView(ListView):
    model = Icone

    def get_context_data(self, **kwargs):
        context = super(IconeListView, self).get_context_data(**kwargs)

        context['normal_32']  = self.model.objects.size_32().normal()
        context['normal_64']  = self.model.objects.size_64().normal()
        context['inverso_32'] = self.model.objects.size_32().inverso()
        context['inverso_64'] = self.model.objects.size_64().inverso()

        context['cores'] = Cor.objects.all()

        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(IconeListView, self).dispatch(request, *args, **kwargs)
