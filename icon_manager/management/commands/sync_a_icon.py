# coding: utf-8
from django.core.files.images import ImageFile
from django.core.management.base import BaseCommand, CommandError
from icon_manager.models import Icone, Cor
from settings import STATIC_ROOT
import os

ROOT_DIR = os.path.abspath(os.path.dirname(__file__))
IMAGENS = os.path.join(STATIC_ROOT, 'icones', 'imagens')

class Command(BaseCommand):
    help = u'Comando para popular a tabela da classe Icone com imagens de ícones no diretório imagens'

    def handle(self, *args, **options):
        try:
            c1 = Cor.objects.get(pk=1)
        except Cor.DoesNotExist:
            raise CommandError(u'Objeto não encontrado.')

        my_path = os.path.join(STATIC_ROOT, 'core', 'img', 'ajuda-inverso.png')
        my_file = ImageFile(open(my_path, 'rb'))

        icone = Icone()
        icone.nome = 'clinicas'
        icone.tamanho = 32
        icone.cor = c1
        icone.imagem = my_file
        icone.ativo = True
        icone.inverso = False
        icone.save()

        self.stdout.write(u'Fim!')
