# coding: utf-8
from django.core.files.images import ImageFile
from django.core.management.base import BaseCommand, CommandError
from icon_manager.models import Icone, Cor
import os, re
from settings import STATIC_ROOT

try:
    from PIL import Image
except ImportError:
    import Image

def main_rgb_color(im):
    return max(im.getcolors(im.size[0] * im.size[1]))[1]

ROOT_DIR = os.path.abspath(os.path.dirname(__file__))
IMAGENS = os.path.join(STATIC_ROOT, 'icones', 'imagens')

class Command(BaseCommand):
    help = u'Comando para popular a tabela da classe Icone com imagens de ícones no diretório imagens'

    def handle(self, *args, **options):
        list_icons = os.listdir(IMAGENS)

        count, my_cor = 0, None
        for icon in list_icons:
            my_path = os.path.join(IMAGENS, icon)
            my_file = ImageFile(open(my_path, 'rb'))
            nome_icone = icon.replace('.png', '').replace('_', '')
            nome_icone = re.sub(r'[\d+]', '', nome_icone)

            im = Image.open(my_path)
            rgb = main_rgb_color(im)

            if not count % 2:
                if rgb == (0, 134, 219):
                    my_cor = Cor.objects.get(nome='azul')
                elif rgb == (229, 1, 10):
                    my_cor = Cor.objects.get(nome='vermelho')
                elif rgb == (68, 76, 86):
                    my_cor = Cor.objects.get(nome='grafite')
                elif rgb == (255, 193, 0):
                    my_cor = Cor.objects.get(nome='amarelo')
                else:
                    my_cor = Cor.objects.get(nome='verde')

            icone32 = Icone()
            icone32.nome = nome_icone.replace('-normal', '').replace('-inverso', '')
            icone32.tamanho = 32
            icone32.cor = my_cor
            icone32.imagem = my_file
            icone32.ativo = True
            icone32.inverso = False if 'normal' in nome_icone else True
            icone32.save()

            self.stdout.write('{0} - {1} - {2}\n'.format(icone32.pk, icone32.nome, icone32.cor.nome))

            icone64 = Icone()
            icone64.nome = nome_icone.replace('-normal', '').replace('-inverso', '')
            icone64.tamanho = 64
            icone64.cor = my_cor
            icone64.imagem = my_file
            icone64.ativo = True
            icone64.inverso = False if 'normal' in nome_icone else True
            icone64.save()

            self.stdout.write('{0} - {1} - {2}\n'.format(icone64.pk, icone64.nome, icone64.cor.nome))
            my_file.close()

            count += 1

        self.stdout.write(u'Tabela da classe ícones populada.')
