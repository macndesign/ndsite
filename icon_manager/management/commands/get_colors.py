from django.core.management.base import BaseCommand
import os
from settings import STATIC_ROOT

try:
    from PIL import Image
except ImportError:
    import Image

ROOT_DIR = os.path.abspath(os.path.dirname(__file__))
IMAGENS = os.path.join(STATIC_ROOT, 'icones', 'imagens')

# Azuis
AJUDA_INVERSO = os.path.join(IMAGENS, 'ajuda-inverso.png') # 0-134-219
ATUALIZAR_INVERSO = os.path.join(IMAGENS, 'atualizar-inverso.png') # 0-134-219
CALENDARIO_INVERSO = os.path.join(IMAGENS, 'calendario-inverso.png') # 0-134-219

# Vermelhos
CANCELAR_INVERSO = os.path.join(IMAGENS, 'cancelar-inverso.png') # 229-1-10
EXCLUIR_INVERSO = os.path.join(IMAGENS, 'excluir-inverso.png') # 229-1-10
FECHAR_INVERSO = os.path.join(IMAGENS, 'fechar-inverso.png') # 229-1-10

# Grafites
DETALHE_INVERSO = os.path.join(IMAGENS, 'detalhe-inverso.png') # 68-76-86
LISTA_INVERSO = os.path.join(IMAGENS, 'lista-inverso.png') # 68-76-86
IMPRIMIR_INVERSO = os.path.join(IMAGENS, 'imprimir-inverso.png') # 68-76-86

# Amarelos
SEGURANCA_INVERSO = os.path.join(IMAGENS, 'seguranca-inverso.png') # 255-193-0
BLOQUEAR_INVERSO = os.path.join(IMAGENS, 'bloquear-inverso.png') # 255-193-0
PASTA_INVERSO = os.path.join(IMAGENS, 'pasta-inverso.png') # 255-193-0

# Verdes
NOVO_INVERSO = os.path.join(IMAGENS, 'novo-inverso.png') # 40-183-0
EDITAR_INVERSO = os.path.join(IMAGENS, 'editar-inverso.png') # 40-183-0
SALVAR_INVERSO = os.path.join(IMAGENS, 'salvar-inverso.png') # 40-183-0

def main_rgb_color(im):
    r, g, b = max(im.getcolors(im.size[0] * im.size[1]))[1]
    print '{0}-{1}-{2}'.format(r, g, b)

class Command(BaseCommand):
    help = 'Get colors'
    def handle(self, *args, **options):
        # Azuis

        im = Image.open(AJUDA_INVERSO)
        main_rgb_color(im)

        im = Image.open(ATUALIZAR_INVERSO)
        main_rgb_color(im)

        im = Image.open(CALENDARIO_INVERSO)
        main_rgb_color(im)

        # Vermelhos

        im = Image.open(CANCELAR_INVERSO)
        main_rgb_color(im)

        im = Image.open(EXCLUIR_INVERSO)
        main_rgb_color(im)

        im = Image.open(FECHAR_INVERSO)
        main_rgb_color(im)

        # Grafites

        im = Image.open(DETALHE_INVERSO)
        main_rgb_color(im)

        im = Image.open(LISTA_INVERSO)
        main_rgb_color(im)

        im = Image.open(IMPRIMIR_INVERSO)
        main_rgb_color(im)

        # Amarelos

        im = Image.open(SEGURANCA_INVERSO)
        main_rgb_color(im)

        im = Image.open(BLOQUEAR_INVERSO)
        main_rgb_color(im)

        im = Image.open(PASTA_INVERSO)
        main_rgb_color(im)

        # Verdes

        im = Image.open(NOVO_INVERSO)
        main_rgb_color(im)

        im = Image.open(EDITAR_INVERSO)
        main_rgb_color(im)

        im = Image.open(SALVAR_INVERSO)
        main_rgb_color(im)
