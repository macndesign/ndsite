# coding: utf-8
from django.core.exceptions import ValidationError
from django.db import models
import re

def validator_is_hex(value):
    if not re.match('^#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$', value):
        raise ValidationError(u'Insira um hexadecimal válido.')


class Cor(models.Model):
    nome = models.SlugField(
        max_length=120,
        help_text=u'A CSS Class de cor é formada por "mc-cor-" + (nome)',
    )
    hexadecimal = models.CharField(
        max_length=7,
        validators=[validator_is_hex],
    )

    class Meta:
        ordering = ['nome']
        verbose_name = 'Cor'
        verbose_name_plural = 'Cores'

    def __unicode__(self):
        return self.nome


class IconeQuerySet(models.query.QuerySet):
    def ativo(self):
        return self.filter(ativo=True)

    def size_32(self):
        return self.filter(tamanho=32)

    def size_64(self):
        return self.filter(tamanho=64)

    def normal(self):
        return self.filter(inverso=False)

    def inverso(self):
        return self.filter(inverso=True)


class IconeManager(models.Manager):
    def get_query_set(self):
        return IconeQuerySet(self.model, using=self._db)

    def ativo(self):
        return self.get_query_set().ativo()

    def size_32(self):
        return self.get_query_set().size_32()

    def size_64(self):
        return self.get_query_set().size_64()

    def normal(self):
        return self.get_query_set().normal()

    def inverso(self):
        return self.get_query_set().inverso()


try:
    from PIL import Image, ImageOps
except ImportError:
    import Image
    import ImageOps

def resize_img(file, size):
    u"""
    Redimensiona imagem enviada para o ícone.
    """
    image = Image.open(file)
    image = ImageOps.fit(image, size, Image.ANTIALIAS)
    image.save(file, 'PNG')


class Icone(models.Model):
    nome = models.SlugField(
        max_length=120,
        help_text=u'A CSS Class de nome do ícone é formada por "mc"- + (nome-do-icone) + - + (tamanho)',
    )
    tamanho = models.PositiveIntegerField(
        help_text=u'A CSS Class de tamanho é formada por "mc-sprite-" + (tamanho)',
        default=32,
    )
    cor = models.ForeignKey('Cor')
    imagem = models.ImageField(upload_to='icon_manager')
    ativo = models.BooleanField(default=True)
    inverso = models.BooleanField(default=False)
    objects = IconeManager()

    def save(self, *args, **kwargs):
        super(Icone, self).save(*args, **kwargs)
        resize_img(
            file = self.imagem.path,
            size = (32, 32) if self.tamanho == 32 else (64, 64),
        )

    def icone_thumb(self):
        u"""
        Mostra ícone na listagem do admin.
        """
        img = '<img src="%(imagem)s" height="%(tamanho)s" width="%(tamanho)s" />' \
        % {'imagem': self.imagem.url, 'tamanho': 32 if self.tamanho == 32 else 64}
        return img

    icone_thumb.allow_tags = True

    class Meta:
        ordering = ['nome']
        verbose_name = u'Ícone'
        verbose_name_plural = u'Ícones'

    def __unicode__(self):
        return self.nome
