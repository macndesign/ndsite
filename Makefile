clean:
	@find . -name "*.pyc" -delete

syncdb:
	@python manage.py syncdb --noinput
	@python manage.py migrate

deps:
	$pip install -r dev_requirements.txt

settings:
	cp ndsite/local_settings.example.py ndsite/local_settings.py

run:
	@python manage.py runserver 0.0.0.0:8000

update:
	@git push heroku master

remote_syncdb:
	@heroku run python manage.py syncdb --noinput
	@heroku run python manage.py migrate

static:
	@heroku run python manage.py collectstatic

loaddemo: syncdb
	@python manage.py loaddata demo/demo_data.json
	@cp -R demo/demo_media media

test:
	coverage erase
	coverage run --include="ndsite/*" manage.py test --with-xunit --settings=ndsite.settings
	coverage xml

help:
	@grep '^[^#[:space:]].*:' Makefile | awk -F ":" '{print $$1}'

setup: deps settings syncdb

deploy: update remote_syncdb static