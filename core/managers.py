# -*- coding: utf-8 -*-
from django.db import models
from django.db.models.query_utils import Q

class MainSliderManager(models.Manager):
    def is_active(self):
        return self.filter(active=True)


class WordNewsManager(models.Manager):
    def is_active(self):
        return self.filter(active=True)


class DirectSaleManager(models.Manager):
    def is_active(self):
        return self.filter(active=True)


class ClientManager(models.Manager):
    def is_active(self):
        return self.filter(active=True)

    def laboratorios_ou_fornecedores_ativos(self):
        return self.is_active().filter(Q(client_type='L') | Q(client_type='F'))

    def clinicas_ou_consultorios_ativos(self):
        return self.is_active().filter(Q(client_type='C') | Q(client_type='O'))


class DistributorManager(models.Manager):
    def is_active(self):
        return self.filter(active=True)


class ProductManager(models.Manager):
    def is_active(self):
        return self.filter(active=True)


class PartnerManager(models.Manager):
    def is_active(self):
        return self.filter(active=True)


class AboutManager(models.Manager):
    def is_active(self):
        return self.filter(active=True)

    def latest_created(self):
        if self.is_active():
            return self.is_active().order_by('-created_date')[0]
        else:
            return None


class ShortNewsManager(models.Manager):
    def is_active(self):
        return self.filter(active=True)

    def last_activate(self):
        """
        Retorna apenas um objeto
        """
        if self.is_active():
            return self.is_active().order_by('-news_date')[0]
        else:
            return None

    def latest_news(self):
        u"""
        Retorna as 15 ultimas que estão ativas
        """
        if self.is_active():
            return self.is_active().order_by('-news_date', '-created_date')[:15]
        else:
            return None
