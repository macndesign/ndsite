# coding: utf-8
from django.contrib.localflavor.br.br_states import STATE_CHOICES
from string import digits
from core.models import DirectSale
from django.core.exceptions import ValidationError

SEPARADOR = '. - , / |'.split()

def validate_max_active_sale(value):
    u"""
    Valida se existem mais vendas diretas ativas do que podem ter em cada dimensão
    """
    qs = DirectSale.objects.all()

    if value:
        # Regras para itens ativos em suas respectivas posições
        if qs.filter(position_type='1'):
            if len(qs.filter(dimension='1/1').filter(active=True)) == 1:
                raise ValidationError(u'Já existe 1 item ativo na dimensão 1/1')

        if qs.filter(position_type='2'):
            if len(qs.filter(dimension='1/2').filter(active=True)) == 2:
                raise ValidationError(u'Já existem 2 itens ativos na dimensão 1/2')

        if qs.filter(position_type='4'):
            if len(qs.filter(dimension='1/4').filter(active=True)) == 4:
                raise ValidationError(u'Já existem 4 itens ativos na dimensão 1/4')


def validate_locality(value):
    u"""
    Verifica se o campo locality está válido.
    """

    lst_upper = [x[0] for x in STATE_CHOICES]
    lst_lower = [x[0].lower() for x in STATE_CHOICES]
    lst_capitalize = [x[0].capitalize() for x in STATE_CHOICES]

    if value[-2:] not in (lst_upper + lst_lower + lst_capitalize):
        raise ValidationError(u'Insira um estado brasileiro válido. Ex: SP')

    separador_list = [letra for letra in value if letra in SEPARADOR]
    if not separador_list:
        raise ValidationError(u'Insira um separador entre a cidade e o estado como: Ex: . - , / |')

    if len(separador_list) > 1:
        raise ValidationError(u'Insira apenas 1 separador entre a cidade e o estado como: Ex: -')

    for letra in value[:-5]:
        if letra in digits:
            raise ValidationError(u'O campo não aceita dígitos.')


def format_locality(self, instance):
    u"""
    Formata o campo locality no save de uma classe Form:

    >>> format_locality(self, 'rio de janeiro,rj')
    ... Rio de Janeiro - RJ
    >>>
    >>> format_locality(self, 'rio de Janeiro.rj')
    ... Rio de Janeiro - RJ
    >>>
    >>> format_locality(self, 'rio de Janeiro | rj')
    ... Rio de Janeiro - RJ

    Formata pegando qualquer um dos separadores válidos,
    inserindo um espaço antes e depois do separador e
    substituindo pelo hífen "-".
    """

    a_str = instance.locality
    sep = [letra for letra in a_str if letra in SEPARADOR][0]
    a_str = a_str.replace(sep, ' ')
    a_str = a_str.split()
    a_str = ' '.join(a_str)
    a_str = u'{0} - {1}'.format(a_str[:-2], a_str[-2:])

    if len(a_str) > 1:
        a_lst = [x if x in ['de', 'do'] else x.capitalize() for x in a_str.split()]
        a_lst[-1] = a_lst[-1].upper()
        a_str = ' '.join(a_lst)

        instance.locality = a_str
