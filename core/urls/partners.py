# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from core.models import Partner
from core.views.partners import PartnerIndexTemplateView, TechnologyListView, HealthListView, PartnerCreateView, PartnerListView, PartnerDetailView, PartnerUpdateView, PartnerDeleteView, AllPartnersListView, PartnerSiteDetailView

urlpatterns = patterns('',
    # Site
    url(r'^escolha/$', PartnerIndexTemplateView.as_view(), name='index'),
    url(r'^tecnologia/$', TechnologyListView.as_view(), name='technology'),
    url(r'^saude/$', HealthListView.as_view(), name='health'),
    url(r'^todos/$', AllPartnersListView.as_view(), name='all'),
    url(r'^parceiro/(?P<pk>\d+)/$', PartnerSiteDetailView.as_view(), name='partner_detail'),

    # Sistema de gerenciamento
    url(r'^create/$', PartnerCreateView.as_view(), name='create'),
    url(r'^list/$', PartnerListView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/detail/$', PartnerDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/$', PartnerUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/$', PartnerDeleteView.as_view(), name='delete'),

    # Ativar/Desativar todos
    url(r'^activate_all/$', 'core.views.utils.activate_or_deactivate_all',
        {'model': Partner, 'activate': True, 'redirect_to': 'partners:list'}, name='activate_all',
    ),
    url(r'^deactivate_all/$', 'core.views.utils.activate_or_deactivate_all',
        {'model': Partner, 'activate': False, 'redirect_to': 'partners:list'}, name='deactivate_all',
    ),
)
