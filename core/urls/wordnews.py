# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from core.models import WordNews
from core.views.wordnews import WordNewsView, WordNewsCreateView, WordNewsListView, WordNewsDetailView, WordNewsUpdateView, WordNewsDeleteView

urlpatterns = patterns('',
    # JSON Dumps de WordNews
    url(r'^json/$', WordNewsView.as_view(), name='json'),
    url(r'^create/$', WordNewsCreateView.as_view(), name='create'),
    url(r'^list/$', WordNewsListView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/detail/$', WordNewsDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/$', WordNewsUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/$', WordNewsDeleteView.as_view(), name='delete'),

    # Ativar/Desativar todos
    url(r'^activate_all/$', 'core.views.utils.activate_or_deactivate_all',
        {'model': WordNews, 'activate': True, 'redirect_to': 'wordnews:list'}, name='activate_all',
    ),
    url(r'^deactivate_all/$', 'core.views.utils.activate_or_deactivate_all',
        {'model': WordNews, 'activate': False, 'redirect_to': 'wordnews:list'}, name='deactivate_all',
    ),
)
