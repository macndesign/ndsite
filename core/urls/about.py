# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from core.models import About
from core.views.about import AboutTemplateView, AboutCreateView, AboutListView, AboutDetailView, AboutUpdateView, AboutDeleteView

urlpatterns = patterns('',
    # Sistema de gerenciamento
    url(r'^create/$', AboutCreateView.as_view(), name='create'),
    url(r'^list/$', AboutListView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/detail/$', AboutDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/$', AboutUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/$', AboutDeleteView.as_view(), name='delete'),

    # Ativar/Desativar todos
    url(r'^activate_all/$', 'core.views.utils.activate_or_deactivate_all',
        {'model': About, 'activate': True, 'redirect_to': 'about:list'}, name='activate_all',
    ),
    url(r'^deactivate_all/$', 'core.views.utils.activate_or_deactivate_all',
        {'model': About, 'activate': False, 'redirect_to': 'about:list'}, name='deactivate_all',
    ),
)
