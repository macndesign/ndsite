# coding: utf-8
from core.views.about import AboutTemplateView
from core.views.clients import ClientsTemplateView
from core.views.distributor import DistributorIndexListView
from core.views.partners import AllPartnersListView
from core.views.products import ProductMoreDetailView, ProductIndexListView
from core.views.shortnews import ShortNewsSiteListView, ShortNewsSiteDetailView
from django.conf.urls import patterns, url
from core.views.base import HomepageView, ManagerView, ContactFormView, PermissionDenied, ContactFormSPView, ContactFormFLView
from icon_manager.views import IconeListView

urlpatterns = patterns('',
    # Homepage
    url(r'^$', HomepageView.as_view(), name='homepage'),
    url(r'^manager/$', ManagerView.as_view(), name='manager'),
    url(r'^permission_denied/$', PermissionDenied.as_view(), name='permission_denied'),
    url(r'^contatos/$', ContactFormView.as_view(), name='contact'),
    url(r'^contatos/br/sp/$', ContactFormSPView.as_view(), name='contact_sp'),
    url(r'^contatos/us/fl/$', ContactFormFLView.as_view(), name='contact_fl'),

    # Sobre
    url(r'^sobre/$', AboutTemplateView.as_view(), name='sobre'),

    # Clientes
    url(r'^clientes/$', ClientsTemplateView.as_view(), name='clientes'),

    # Parceiros
    url(r'^parceiros/$', AllPartnersListView.as_view(), name='parceiros'),

    # Distribuidores
    url(r'^distribuidores/$', DistributorIndexListView.as_view(), name='distribuidores'),

    # Services
    url(r'^servicos/$', ProductIndexListView.as_view(), name='services'),
    url(r'^(?P<slug>[\w_-]+)/$', ProductMoreDetailView.as_view(), name='service'),

    # Noticias
    url(r'^noticias/todas/$', ShortNewsSiteListView.as_view(), name='news'),
    url(r'^noticia/(?P<slug>[\w_-]+)/$', ShortNewsSiteDetailView.as_view(), name='news_detail'),

    # Icon Manager
    url(r'^icon/list/$', IconeListView.as_view(), name='icons'),
)
