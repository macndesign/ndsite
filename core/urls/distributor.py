# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from core.models import  Distributor
from core.views.distributor import DistributorIndexListView, DistributorCreateView, DistributorListView, DistributorDetailView, DistributorUpdateView, DistributorDeleteView

urlpatterns = patterns('',
    # Site
    url(r'^todos/$', DistributorIndexListView.as_view(), name='index'),

    # Sistema de gerenciamento
    url(r'^create/$', DistributorCreateView.as_view(), name='create'),
    url(r'^list/', DistributorListView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/detail/$', DistributorDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/$', DistributorUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/$', DistributorDeleteView.as_view(), name='delete'),

    # Ativar/Desativar todos
    url(r'^activate_all/$', 'core.views.utils.activate_or_deactivate_all',
        {'model': Distributor, 'activate': True, 'redirect_to': 'distributor:list'}, name='activate_all',
    ),
    url(r'^deactivate_all/$', 'core.views.utils.activate_or_deactivate_all',
        {'model': Distributor, 'activate': False, 'redirect_to': 'distributor:list'}, name='deactivate_all',
    ),
)
