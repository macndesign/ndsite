# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from core.models import DirectSale
from core.views.directsale import DirectSaleCreateView, DirectSaleListView, DirectSaleDetailView, DirectSaleUpdateView, DirectSaleDeleteView

urlpatterns = patterns('',
    url(r'^create/$', DirectSaleCreateView.as_view(), name='create'),
    url(r'^list/$', DirectSaleListView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/detail/$', DirectSaleDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/$', DirectSaleUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/$', DirectSaleDeleteView.as_view(), name='delete'),
    url(r'^(?P<type>\w+)/change-position/$', 'core.views.directsale.change_position_type', name='change_position'),

    # Ativar/Desativar todos
    url(r'^activate_all/$', 'core.views.utils.activate_or_deactivate_all',
        {'model': DirectSale, 'activate': True, 'redirect_to': 'directsale:list'}, name='activate_all',
    ),
    url(r'^deactivate_all/$', 'core.views.utils.activate_or_deactivate_all',
        {'model': DirectSale, 'activate': False, 'redirect_to': 'directsale:list'}, name='deactivate_all',
    ),
)
