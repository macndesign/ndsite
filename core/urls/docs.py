# coding: utf-8
from django.conf.urls import patterns, url
from core.views.docs import DocsSoftClinicRedirectView

urlpatterns = patterns('',
   # Docs SoftClinic
   url(r'^softclinic/$', DocsSoftClinicRedirectView.as_view(),
       name='docs-softclinic'),
)
