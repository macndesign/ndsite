# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from core.models import MainSlider
from core.views.mainslider import MainSliderListView, MainSliderCreateView, MainSliderDetailView, MainSliderUpdateView, MainSliderDeleteView

urlpatterns = patterns('',
    url(r'^create/$', MainSliderCreateView.as_view(), name='create'),
    url(r'^list/$', MainSliderListView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/detail/$', MainSliderDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/$', MainSliderUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/$', MainSliderDeleteView.as_view(), name='delete'),
    
    # Ativar/Desativar todos
    url(r'^activate_all/$', 'core.views.utils.activate_or_deactivate_all',
        {'model': MainSlider, 'activate': True, 'redirect_to': 'mainslider:list'}, name='activate_all',
    ),
    url(r'^deactivate_all/$', 'core.views.utils.activate_or_deactivate_all',
        {'model': MainSlider, 'activate': False, 'redirect_to': 'mainslider:list'}, name='deactivate_all',
    ),
)
