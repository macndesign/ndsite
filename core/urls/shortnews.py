# coding: utf-8
from django.conf.urls import patterns, url
from core.models import ShortNews
from core.views.shortnews import ShortNewsDeleteView, ShortNewsUpdateView, ShortNewsDetailView, ShortNewsListView, ShortNewsCreateView, ShortNewsSiteListView, ShortNewsSiteDetailView

urlpatterns = patterns('',
    # Site
    url(r'^news/$', ShortNewsSiteListView.as_view(), name='news'),
    url(r'^news/(?P<slug>[\w_-]+)/$', ShortNewsSiteDetailView.as_view(), name='news_detail'),
    # Sistema de gerenciamento
    url(r'^create/$', ShortNewsCreateView.as_view(), name='create'),
    url(r'^list/$', ShortNewsListView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/detail/$', ShortNewsDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/$', ShortNewsUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/$', ShortNewsDeleteView.as_view(), name='delete'),

    # Ativar/Desativar todos
    url(r'^activate_all/$', 'core.views.utils.activate_or_deactivate_all',
            {'model': ShortNews, 'activate': True, 'redirect_to': 'shortnews:list'}, name='activate_all',
    ),
    url(r'^deactivate_all/$', 'core.views.utils.activate_or_deactivate_all',
            {'model': ShortNews, 'activate': False, 'redirect_to': 'shortnews:list'}, name='deactivate_all',
    ),
)
