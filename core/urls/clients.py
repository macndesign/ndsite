# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from core.models import Client
from core.views.clients import ClientsTemplateView, ClientCreateView, ClientListView, ClientDetailView, ClientUpdateView, ClientDeleteView, LabsListView, ClinicListView, OfficeListView, ProviderListView

urlpatterns = patterns('',
    # Site
    url(r'^escolha/$', ClientsTemplateView.as_view(), name='index'),
    url(r'^laboratorios/$', LabsListView.as_view(), name='labs'),
    url(r'^clinicas/$', ClinicListView.as_view(), name='clinic'),
    url(r'^consultorios/$', OfficeListView.as_view(), name='office'),
    url(r'^fornecedores/$', ProviderListView.as_view(), name='provider'),

    # Sistema de gerenciamento
    url(r'^create/$', ClientCreateView.as_view(), name='create'),
    url(r'^list/$', ClientListView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/detail/$', ClientDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/$', ClientUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/$', ClientDeleteView.as_view(), name='delete'),

    # Ativar/Desativar todos
    url(r'^activate_all/$', 'core.views.utils.activate_or_deactivate_all',
        {'model': Client, 'activate': True, 'redirect_to': 'clients:list'}, name='activate_all',
    ),
    url(r'^deactivate_all/$', 'core.views.utils.activate_or_deactivate_all',
        {'model': Client, 'activate': False, 'redirect_to': 'clients:list'}, name='deactivate_all',
    ),
)
