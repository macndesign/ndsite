# -*- coding: utf-8 -*-
from core.models import Product
from core.views.products import ProductCreateView, ProductListView, ProductDetailView, ProductUpdateView, ProductDeleteView
from django.conf.urls import patterns, url

# Products
urlpatterns = patterns('',
    # Sistema de gerenciamento
    url(r'^create/$', ProductCreateView.as_view(), name='create'),
    url(r'^list/$', ProductListView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/detail/$', ProductDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/$', ProductUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/$', ProductDeleteView.as_view(), name='delete'),

    # Ativar/Desativar todos
    url(r'^activate_all/$', 'core.views.utils.activate_or_deactivate_all',
            {'model': Product, 'activate': True, 'redirect_to': 'products:list'}, name='activate_all',
    ),
    url(r'^deactivate_all/$', 'core.views.utils.activate_or_deactivate_all',
            {'model': Product, 'activate': False, 'redirect_to': 'products:list'}, name='deactivate_all',
    ),
)