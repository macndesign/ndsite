# -*- coding: utf-8 -*-
from django.contrib import messages
from django.core import serializers
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from core.forms import WordNewsForm
from core.models import WordNews
from core.views.utils import generic_fields_key_value

class WordNewsView(TemplateView):
    def get(self, request, *args, **kwargs):
        """
        JSON Dumps de WordNews
        """
        json_serializer = serializers.get_serializer("json")()
        queryset = WordNews.objects.is_active()
        return HttpResponse(json_serializer.serialize(queryset, ensure_ascii=False), mimetype='application/json')

class WordNewsListView(ListView):
    model = WordNews
    context_object_name = 'wordnews'
    template_name = 'wordnews/list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(WordNewsListView, self).dispatch(request, *args, **kwargs)

class WordNewsCreateView(CreateView):
    model = WordNews
    form_class = WordNewsForm
    template_name = 'wordnews/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Letreiro <strong>%s</strong> cadastrado com sucesso.' % self.object.provider)
        return reverse_lazy('wordnews:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(WordNewsCreateView, self).form_invalid(form)

    @method_decorator(permission_required('core.add_wordnews', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(WordNewsCreateView, self).dispatch(request, *args, **kwargs)

class WordNewsUpdateView(UpdateView):
    model = WordNews
    form_class = WordNewsForm
    context_object_name = 'wordnews'
    template_name = 'wordnews/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Letreiro <strong style="color:#2C6ED5">%s</strong> alterado com sucesso.' % self.object.provider)
        return reverse_lazy('wordnews:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(WordNewsUpdateView, self).form_invalid(form)

    @method_decorator(permission_required('core.change_wordnews', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(WordNewsUpdateView, self).dispatch(request, *args, **kwargs)

class WordNewsDetailView(DetailView):
    model = WordNews
    context_object_name = 'wordnews'
    template_name = 'base/detail.html'

    def get_context_data(self, **kwargs):
        context = super(WordNewsDetailView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(WordNewsDetailView, self).dispatch(request, *args, **kwargs)

class WordNewsDeleteView(DeleteView):
    model = WordNews
    context_object_name = 'wordnews'
    template_name = 'base/delete.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Letreiro <strong style="color:#CC3434">%s</strong> excluído com sucesso.' % self.object.provider)
        return reverse_lazy('wordnews:list')

    def get_context_data(self, **kwargs):
        context = super(WordNewsDeleteView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(permission_required('core.delete_wordnews', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(WordNewsDeleteView, self).dispatch(request, *args, **kwargs)
