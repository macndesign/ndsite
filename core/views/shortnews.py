# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from core.forms import ShortNewsForm
from core.models import ShortNews
from core.views.utils import generic_fields_key_value

# Site
class ShortNewsSiteListView(ListView):
    model = ShortNews
    context_object_name = 'news_list'
    template_name = 'shortnews/news_list.html'
    queryset = ShortNews.objects.latest_news()

class ShortNewsSiteDetailView(DetailView):
    model = ShortNews
    context_object_name = 'shortnews'
    template_name = 'shortnews/news_detail.html'
    queryset = ShortNews.objects.is_active()

    def get_context_data(self, **kwargs):
        context = super(ShortNewsSiteDetailView, self).get_context_data(**kwargs)
        news_list = ShortNews.objects.latest_news()
        context['news_list'] = news_list
        
        return context

# Sistema de gerenciamento
class ShortNewsListView(ListView):
    model = ShortNews
    context_object_name = 'shortnews_list'
    template_name = 'shortnews/list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ShortNewsListView, self).dispatch(request, *args, **kwargs)

class ShortNewsCreateView(CreateView):
    model = ShortNews
    form_class = ShortNewsForm
    template_name = 'shortnews/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Notícia curta cadastrada com sucesso.')
        return reverse_lazy('shortnews:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(ShortNewsCreateView, self).form_invalid(form)

    @method_decorator(permission_required('core.add_shortnews', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(ShortNewsCreateView, self).dispatch(request, *args, **kwargs)

class ShortNewsUpdateView(UpdateView):
    model = ShortNews
    form_class = ShortNewsForm
    template_name = 'shortnews/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Notícia curta alterada com sucesso.')
        return reverse_lazy('shortnews:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(ShortNewsUpdateView, self).form_invalid(form)

    @method_decorator(permission_required('core.change_shortnews', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(ShortNewsUpdateView, self).dispatch(request, *args, **kwargs)

class ShortNewsDetailView(DetailView):
    model = ShortNews
    context_object_name = 'shortnews'
    template_name = 'base/detail.html'

    def get_context_data(self, **kwargs):
        context = super(ShortNewsDetailView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ShortNewsDetailView, self).dispatch(request, *args, **kwargs)

class ShortNewsDeleteView(DeleteView):
    model = ShortNews
    context_object_name = 'shortnews'
    template_name = 'base/delete.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Notícia curta excluída.')
        return reverse_lazy('shortnews:list')

    def get_context_data(self, **kwargs):
        context = super(ShortNewsDeleteView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(permission_required('core.delete_shortnews', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(ShortNewsDeleteView, self).dispatch(request, *args, **kwargs)
