# coding: utf-8
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.shortcuts import redirect

def generic_fields_key_value(self, field_keys=None, field_values=None):
    """
    Generaliza as telas de detalhar e excluir.
    Gera campos e valores do objeto para o template.
    """
    
    for k, v in self.object.__dict__.items():
        for i in self.object._meta.fields:
            if k != '_state' and k != 'id' and i.attname == k:
                field_keys.append(i.verbose_name)
                field_values.append(v)

    return field_keys, field_values


def activate_or_deactivate_all(request, model, activate, redirect_to):
    u"""
    Função genérica para ativar ou desativar objetos que contém o atributo
    'active' de tipo boolean.
    """

    objects = model.objects.all()

    if objects:
        for object in objects:
            if activate:
                if not object.active:
                    object.active = True
            else:
                if object.active:
                    object.active = False

            object.save()

        if activate:
            messages.add_message(request, messages.SUCCESS, u'Todos estão ativos.')
        else:
            messages.add_message(request, messages.SUCCESS, u'Todos estão inativos.')
    else:
        messages.add_message(request, messages.INFO, u'Não há itens.')

    return redirect(reverse(redirect_to))


def distinct_list_items(qs, field=''):
    u"""
    Função genérica para distinct e order_by pelo campo informado.
    Retorna uma lista de itens.
    """

    items = sorted(list(set([getattr(item, field) for item in qs])))

    item_dict, item_list = {}, []
    for k, v in enumerate(items):
        item_dict = {field: items[k]}
        item_list.append(item_dict)

    return item_list
