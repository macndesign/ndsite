# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from core.forms import PartnerForm
from core.models import Partner
from core.views.utils import generic_fields_key_value, distinct_list_items

# Site
class PartnerIndexTemplateView(TemplateView):
    template_name = 'partners/index.html'

class TechnologyListView(ListView):
    template_name = 'partners/technology.html'
    model = Partner
    context_object_name = 'partner_list'
    queryset = Partner.objects.is_active().filter(partner_type='T')

class HealthListView(ListView):
    template_name = 'partners/health.html'
    model = Partner
    context_object_name = 'partner_list'
    queryset = Partner.objects.is_active().filter(partner_type='S')

class AllPartnersListView(ListView):
    template_name = 'partners/all.html'
    model = Partner
    context_object_name = 'partner_list'

    def get_queryset(self):
        locality = self.request.GET.get('locality', '')
        return Partner.objects.is_active().filter(locality__iregex='%s$' % locality)

    def get_context_data(self, **kwargs):
        context = super(AllPartnersListView, self).get_context_data(**kwargs)

        qs = self.model.objects.is_active()
        locality_list = distinct_list_items(qs, 'locality')

        state_list = []
        for i in locality_list:
            state_list.append(i['locality'][-2:])

        state_list, new_state_list = sorted(list(set(state_list))), []
        for k, v in enumerate(state_list):
            new_state_list.append({'locality': state_list[k]})

        context['new_state_list'] = new_state_list

        return context

class PartnerSiteDetailView(DetailView):
    template_name = 'partners/detail.html'
    model = Partner
    context_object_name = 'partner'

# Sistema de gerenciamento
class PartnerListView(ListView):
    model = Partner
    context_object_name = 'partners'
    template_name = 'partners/list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(PartnerListView, self).dispatch(request, *args, **kwargs)

class PartnerCreateView(CreateView):
    model = Partner
    form_class = PartnerForm
    template_name = 'partners/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Parceiro cadastrado com sucesso.')
        return reverse_lazy('partners:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(PartnerCreateView, self).form_invalid(form)

    @method_decorator(permission_required('core.add_partner', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(PartnerCreateView, self).dispatch(request, *args, **kwargs)

class PartnerUpdateView(UpdateView):
    model = Partner
    form_class = PartnerForm
    template_name = 'partners/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Parceiro alterado com sucesso.')
        return reverse_lazy('partners:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(PartnerUpdateView, self).form_invalid(form)

    @method_decorator(permission_required('core.change_partner', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(PartnerUpdateView, self).dispatch(request, *args, **kwargs)

class PartnerDetailView(DetailView):
    model = Partner
    context_object_name = 'partner'
    template_name = 'base/detail.html'

    def get_context_data(self, **kwargs):
        context = super(PartnerDetailView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(PartnerDetailView, self).dispatch(request, *args, **kwargs)

class PartnerDeleteView(DeleteView):
    model = Partner
    context_object_name = 'partner'
    template_name = 'base/delete.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Parceiro excluído.')
        return reverse_lazy('partners:list')

    def get_context_data(self, **kwargs):
        context = super(PartnerDeleteView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(permission_required('core.delete_partner', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(PartnerDeleteView, self).dispatch(request, *args, **kwargs)
