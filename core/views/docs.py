from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic.base import TemplateView


class DocsSoftClinicRedirectView(TemplateView):
    template_name = 'docs/softclinic.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DocsSoftClinicRedirectView, self).dispatch(request, *args, **kwargs)
