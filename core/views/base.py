# -*- coding: utf-8 -*-
from random import randint
from django.conf import settings
from django.contrib import messages
from django.core.mail.message import EmailMessage
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.template.loader import render_to_string
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from core.forms import ContactForm
from core.models import WordNews, MainSlider, DirectSale, Product, ShortNews
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


class HomepageView(TemplateView):
    template_name = 'base/homepage.html'

    def get_context_data(self, **kwargs):
        u"""
        Variáveis de context para slider e letreiro.
        """

        # Cria o objeto contexto que receberá as variáveis da página
        context = super(HomepageView, self).get_context_data(**kwargs)

        count_nivo = MainSlider.objects.is_active().count()
        rand_nivo = randint(0, count_nivo - 1)
        context['rand_nivo'] = rand_nivo

        count_wn = WordNews.objects.is_active().count()
        rand_wn = randint(0, count_wn - 1)
        context['rand_wn'] = rand_wn

        sliders = MainSlider.objects.is_active()
        context['sliders'] = sliders

        products = Product.objects.is_active()

        # Verifica formatação atual
        direct_sales = DirectSale.objects.is_active()[:DirectSale.objects.all()[0].position_type]
        context['direct_sales'] = direct_sales

        direct_sales4 = DirectSale.objects.is_active().filter(dimension='1/4')[:DirectSale.objects.all()[0].position_type]
        context['direct_sales4'] = direct_sales4

        direct_sales2 = DirectSale.objects.is_active().filter(dimension='1/2')[:DirectSale.objects.all()[0].position_type]
        context['direct_sales2'] = direct_sales2

        direct_sales1 = DirectSale.objects.is_active().filter(dimension='1/1')[:DirectSale.objects.all()[0].position_type]
        context['direct_sales1'] = direct_sales1

        context['products'] = products

        if ShortNews.objects.is_active().count():
            shortnews = ShortNews.objects.last_activate()
            context['shortnews'] = shortnews

        return context


class ManagerView(TemplateView):
    u"""
    Classe de controller da interface de gerenciamento do site.
    """
    template_name = 'base/index.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ManagerView, self).dispatch(request, *args, **kwargs)


class PermissionDenied(TemplateView):
    template_name = 'core/permission_denied.html'


class ContactFormView(FormView):
    u"""
    Controller que valida, anexa arquivos e envia email de contato do site.
    """

    form_class = ContactForm
    template_name = 'core/contact.html'

    def get_success_url(self):
        return reverse_lazy('base:contact')
    
    def form_valid(self, form):
        u"""
        Prepara email para envio;
        Gera template em html com imagens para o email;
        Cria o objeto de envio do email;
        E tenta enviar.
        """
        name = form.cleaned_data['name']
        email = form.cleaned_data['email']
        subject = form.cleaned_data['subject']
        message = form.cleaned_data['message']
        mail = render_to_string(
            'core/email.html', {
                'name': name,
                'email': email,
                'subject': subject,
                'message': message,
            }
        )

        msg = EmailMessage(
            subject = '{} para {}'.format(settings.EMAIL_SUBJECT_PREFIX, subject),
            body = mail,
            from_email = email,
            to = [settings.DEFAULT_FROM_EMAIL],
        )

        msg.content_subtype = 'html'

        try:
            msg.send()
            messages.add_message(self.request, messages.SUCCESS, u'Mensagem enviada para ND Engenharia e Software.')
        except Exception:
            messages.add_message(self.request, messages.ERROR, u'Erro: Sua mensagem não pôde ser enviada.')

        return HttpResponseRedirect(reverse_lazy('base:contact'))

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido. Todos os campos são requeridos.')
        return self.render_to_response(self.get_context_data(form=form))


class ContactFormSPView(ContactFormView):
    def get_context_data(self, **kwargs):
        context = super(ContactFormSPView, self).get_context_data(**kwargs)
        context['SP'] = True
        return context


class ContactFormFLView(ContactFormView):
    def get_context_data(self, **kwargs):
        context = super(ContactFormFLView, self).get_context_data(**kwargs)
        context['FL'] = True
        return context
