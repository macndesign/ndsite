# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from core.forms import DistributorForm
from core.models import Distributor

# Site
from core.views.utils import generic_fields_key_value, distinct_list_items

class DistributorIndexListView(ListView):
    template_name = 'distributor/index.html'
    model = Distributor
    context_object_name = 'distributors'

    def get_queryset(self):
        state = self.request.GET.get('state', '')
        return Distributor.objects.is_active().filter(state__icontains=state)

    def get_context_data(self, **kwargs):
        context = super(DistributorIndexListView, self).get_context_data(**kwargs)

        qs = self.model.objects.is_active()
        context['state_list'] = distinct_list_items(qs, 'state')

        return context

# Sistema de gerenciamento
class DistributorListView(ListView):
    model = Distributor
    context_object_name = 'distributors'
    template_name = 'distributor/list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DistributorListView, self).dispatch(request, *args, **kwargs)

class DistributorCreateView(CreateView):
    model = Distributor
    form_class = DistributorForm
    template_name = 'distributor/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Distribuidor cadastrado com sucesso.')
        return reverse_lazy('distributor:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(DistributorCreateView, self).form_invalid(form)

    @method_decorator(permission_required('core.add_distributor', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(DistributorCreateView, self).dispatch(request, *args, **kwargs)

class DistributorUpdateView(UpdateView):
    model = Distributor
    form_class = DistributorForm
    template_name = 'distributor/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Distribuidor alterado com sucesso.')
        return reverse_lazy('distributor:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(DistributorUpdateView, self).form_invalid(form)

    @method_decorator(permission_required('core.change_distributor', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(DistributorUpdateView, self).dispatch(request, *args, **kwargs)

class DistributorDetailView(DetailView):
    model = Distributor
    context_object_name = 'distributors'
    template_name = 'base/detail.html'

    def get_context_data(self, **kwargs):
        context = super(DistributorDetailView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DistributorDetailView, self).dispatch(request, *args, **kwargs)

class DistributorDeleteView(DeleteView):
    model = Distributor
    context_object_name = 'distributor'
    template_name = 'base/delete.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Distribuidor excluído.')
        return reverse_lazy('distributor:list')

    def get_context_data(self, **kwargs):
        context = super(DistributorDeleteView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(permission_required('core.delete_distributor', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(DistributorDeleteView, self).dispatch(request, *args, **kwargs)
