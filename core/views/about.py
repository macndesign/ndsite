# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from core.forms import AboutForm
from core.models import About
from core.views.utils import generic_fields_key_value

class AboutTemplateView(TemplateView):
    template_name = 'about/business.html'

    def get_context_data(self, **kwargs):
        context = super(AboutTemplateView, self).get_context_data(**kwargs)

        about = About.objects.latest_created()
        context['about'] = about

        return context

# Sistema de gerenciamento
class AboutListView(ListView):
    model = About
    context_object_name = 'abouts'
    template_name = 'about/list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AboutListView, self).dispatch(request, *args, **kwargs)

class AboutCreateView(CreateView):
    model = About
    form_class = AboutForm
    template_name = 'about/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Sobre cadastrado com sucesso.')
        return reverse_lazy('about:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(AboutCreateView, self).form_invalid(form)

    @method_decorator(permission_required('core.add_about', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(AboutCreateView, self).dispatch(request, *args, **kwargs)

class AboutUpdateView(UpdateView):
    model = About
    form_class = AboutForm
    template_name = 'about/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Sobre alterado com sucesso.')
        return reverse_lazy('about:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(AboutUpdateView, self).form_invalid(form)

    @method_decorator(permission_required('core.change_about', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(AboutUpdateView, self).dispatch(request, *args, **kwargs)

class AboutDetailView(DetailView):
    model = About
    context_object_name = 'about'
    template_name = 'base/detail.html'

    def get_context_data(self, **kwargs):
        context = super(AboutDetailView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AboutDetailView, self).dispatch(request, *args, **kwargs)

class AboutDeleteView(DeleteView):
    model = About
    context_object_name = 'about'
    template_name = 'base/delete.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Sobre excluído.')
        return reverse_lazy('about:list')

    def get_context_data(self, **kwargs):
        context = super(AboutDeleteView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(permission_required('core.delete_about', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(AboutDeleteView, self).dispatch(request, *args, **kwargs)
