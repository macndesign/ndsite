# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from core.forms import ClientForm
from core.models import Client
from core.views.utils import generic_fields_key_value, distinct_list_items

class ClientsTemplateView(TemplateView):
    template_name = 'clients/index.html'

class LabsListView(ListView):
    u"""
    Visualizar laboratórios.
    """
    model = Client
    context_object_name = 'clients'
    template_name = 'clients/labs.html'

    def get_queryset(self):
        locality = self.request.GET.get('locality', '')
        return Client.objects.laboratorios_ou_fornecedores_ativos().filter(locality__iregex='%s$' % locality)

    def get_context_data(self, **kwargs):
        context = super(LabsListView, self).get_context_data(**kwargs)

        qs = self.model.objects.laboratorios_ou_fornecedores_ativos()
        locality_list = distinct_list_items(qs, 'locality')

        state_list = []
        for i in locality_list:
            state_list.append(i['locality'][-2:])

        state_list, new_state_list = sorted(list(set(state_list))), []
        for k, v in enumerate(state_list):
            new_state_list.append({'locality': state_list[k]})

        context['new_state_list'] = new_state_list

        return context

class ClinicListView(ListView):
    u"""
    Visualizar laboratórios.
    """
    model = Client
    context_object_name = 'clients'
    template_name = 'clients/clinic.html'

    def get_queryset(self):
        locality = self.request.GET.get('locality', '')
        return Client.objects.clinicas_ou_consultorios_ativos().filter(locality__iregex='%s$' % locality)

    def get_context_data(self, **kwargs):
        context = super(ClinicListView, self).get_context_data(**kwargs)

        qs = self.model.objects.clinicas_ou_consultorios_ativos()
        locality_list = distinct_list_items(qs, 'locality')

        state_list = []
        for i in locality_list:
            state_list.append(i['locality'][-2:])

        state_list, new_state_list = sorted(list(set(state_list))), []
        for k, v in enumerate(state_list):
            new_state_list.append({'locality': state_list[k]})

        context['new_state_list'] = new_state_list

        return context

class OfficeListView(ListView):
    u"""
    Visualizar Consultórios.
    """
    model = Client
    context_object_name = 'clients'
    template_name = 'clients/office.html'

    def get_queryset(self):
        locality = self.request.GET.get('locality', '')
        return Client.objects.is_active().filter(client_type='O').filter(locality__iregex='%s$' % locality)

    def get_context_data(self, **kwargs):
        context = super(OfficeListView, self).get_context_data(**kwargs)

        qs = self.model.objects.is_active().filter(client_type='O')
        locality_list = distinct_list_items(qs, 'locality')

        state_list = []
        for i in locality_list:
            state_list.append(i['locality'][-2:])

        state_list, new_state_list = sorted(list(set(state_list))), []
        for k, v in enumerate(state_list):
            new_state_list.append({'locality': state_list[k]})

        context['new_state_list'] = new_state_list

        return context

class ProviderListView(ListView):
    u"""
    Visualizar fornecedores.
    """
    model = Client
    context_object_name = 'clients'
    template_name = 'clients/provider.html'

    def get_queryset(self):
        locality = self.request.GET.get('locality', '')
        return Client.objects.is_active().filter(client_type='F').filter(locality__iregex='%s$' % locality)

    def get_context_data(self, **kwargs):
        context = super(ProviderListView, self).get_context_data(**kwargs)

        qs = self.model.objects.is_active().filter(client_type='F')
        locality_list = distinct_list_items(qs, 'locality')

        state_list = []
        for i in locality_list:
            state_list.append(i['locality'][-2:])

        state_list, new_state_list = sorted(list(set(state_list))), []
        for k, v in enumerate(state_list):
            new_state_list.append({'locality': state_list[k]})

        context['new_state_list'] = new_state_list

        return context

# Sistema de gerenciamento
class ClientListView(ListView):
    model = Client
    context_object_name = 'clients'
    template_name = 'clients/list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ClientListView, self).dispatch(request, *args, **kwargs)

class ClientCreateView(CreateView):
    model = Client
    form_class = ClientForm
    template_name = 'clients/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Cliente cadastrado com sucesso.')
        return reverse_lazy('clients:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(ClientCreateView, self).form_invalid(form)

    @method_decorator(permission_required('core.add_client', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(ClientCreateView, self).dispatch(request, *args, **kwargs)

class ClientUpdateView(UpdateView):
    model = Client
    form_class = ClientForm
    template_name = 'clients/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Cliente alterado com sucesso.')
        return reverse_lazy('clients:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(ClientUpdateView, self).form_invalid(form)

    @method_decorator(permission_required('core.change_client', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(ClientUpdateView, self).dispatch(request, *args, **kwargs)

class ClientDetailView(DetailView):
    model = Client
    context_object_name = 'slider'
    template_name = 'base/detail.html'

    def get_context_data(self, **kwargs):
        context = super(ClientDetailView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ClientDetailView, self).dispatch(request, *args, **kwargs)

class ClientDeleteView(DeleteView):
    model = Client
    context_object_name = 'slider'
    template_name = 'base/delete.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Cliente excluído.')
        return reverse_lazy('clients:list')

    def get_context_data(self, **kwargs):
        context = super(ClientDeleteView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(permission_required('core.delete_client', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(ClientDeleteView, self).dispatch(request, *args, **kwargs)
