# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from core.forms import MainSliderForm
from core.models import MainSlider
from core.views.utils import generic_fields_key_value

class MainSliderListView(ListView):
    model = MainSlider
    context_object_name = 'sliders'
    template_name = 'mainslider/list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(MainSliderListView, self).dispatch(request, *args, **kwargs)

class MainSliderCreateView(CreateView):
    model = MainSlider
    form_class = MainSliderForm
    template_name = 'mainslider/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Slider cadastrado com sucesso.')
        return reverse_lazy('mainslider:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(MainSliderCreateView, self).form_invalid(form)

    @method_decorator(permission_required('core.add_mainslider', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(MainSliderCreateView, self).dispatch(request, *args, **kwargs)

class MainSliderUpdateView(UpdateView):
    model = MainSlider
    form_class = MainSliderForm
    template_name = 'mainslider/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Slider alterado com sucesso.')
        return reverse_lazy('mainslider:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(MainSliderUpdateView, self).form_invalid(form)

    @method_decorator(permission_required('core.change_mainslider', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(MainSliderUpdateView, self).dispatch(request, *args, **kwargs)

class MainSliderDetailView(DetailView):
    model = MainSlider
    context_object_name = 'slider'
    template_name = 'base/detail.html'

    def get_context_data(self, **kwargs):
        context = super(MainSliderDetailView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(MainSliderDetailView, self).dispatch(request, *args, **kwargs)

class MainSliderDeleteView(DeleteView):
    model = MainSlider
    context_object_name = 'slider'
    template_name = 'base/delete.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Slider excluído.')
        return reverse_lazy('mainslider:list')

    def get_context_data(self, **kwargs):
        context = super(MainSliderDeleteView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(permission_required('core.delete_mainslider', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(MainSliderDeleteView, self).dispatch(request, *args, **kwargs)
