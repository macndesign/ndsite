# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from core.forms import DirectSaleForm, DirectSaleCreateForm
from core.models import DirectSale
from core.views.utils import generic_fields_key_value

class DirectSaleListView(ListView):
    model = DirectSale
    context_object_name = 'directsales'
    template_name = 'directsale/list.html'

    def get_queryset(self):
        # Se adequa a quantidade de anuncios em venda direta
        pt = DirectSale.objects.all()[:DirectSale.objects.all()[0].position_type]
        if len(pt) == 1:
            return DirectSale.objects.filter(dimension='1/1').order_by('position')
        elif len(pt) == 2:
            return DirectSale.objects.filter(dimension='1/2').order_by('position')
        elif len(pt) == 4:
            return DirectSale.objects.filter(dimension='1/4').order_by('position')
        else:
            return pt
    
    def get_context_data(self, **kwargs):
        context = super(DirectSaleListView, self).get_context_data(**kwargs)

        # Variáveis para estilizar links de anuncios
        if DirectSale.objects.latest('pk').position_type == '1':
            context['type_1'] = 'type_1'
        elif DirectSale.objects.latest('pk').position_type == '2':
            context['type_2'] = 'type_2'
        else:
            context['type_4'] = 'type_4'
        
        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DirectSaleListView, self).dispatch(request, *args, **kwargs)

class DirectSaleCreateView(CreateView):
    model = DirectSale
    form_class = DirectSaleCreateForm
    template_name = 'directsale/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Venda direta cadastrada com sucesso.')
        return reverse_lazy('directsale:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(DirectSaleCreateView, self).form_invalid(form)

    @method_decorator(permission_required('core.add_directsale', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(DirectSaleCreateView, self).dispatch(request, *args, **kwargs)

class DirectSaleUpdateView(UpdateView):
    model = DirectSale
    form_class = DirectSaleForm
    template_name = 'directsale/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Venda direta alterado com sucesso.')
        return reverse_lazy('directsale:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(DirectSaleUpdateView, self).form_invalid(form)

    @method_decorator(permission_required('core.change_directsale', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(DirectSaleUpdateView, self).dispatch(request, *args, **kwargs)

class DirectSaleDetailView(DetailView):
    model = DirectSale
    context_object_name = 'directsale'
    template_name = 'base/detail.html'

    def get_context_data(self, **kwargs):
        context = super(DirectSaleDetailView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DirectSaleDetailView, self).dispatch(request, *args, **kwargs)

class DirectSaleDeleteView(DeleteView):
    model = DirectSale
    context_object_name = 'directsale'
    template_name = 'base/delete.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Venda direta excluído.')
        return reverse_lazy('directsale:list')

    def get_context_data(self, **kwargs):
        context = super(DirectSaleDeleteView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(permission_required('core.delete_directsale', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(DirectSaleDeleteView, self).dispatch(request, *args, **kwargs)

@login_required
def change_position_type(request, type):
    direct_sales = DirectSale.objects.all()
    for ds in direct_sales:
        ds.position_type = type
        ds.save()

    return HttpResponseRedirect(reverse_lazy('directsale:list'))
