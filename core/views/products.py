# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from core.forms import ProductForm
from core.models import Product

# Site
from core.views.utils import generic_fields_key_value

class ProductIndexListView(ListView):
    template_name = 'products/index.html'
    model = Product
    context_object_name = 'products'
    queryset = Product.objects.is_active()

class ProductMoreDetailView(DetailView):
    template_name = 'products/more.html'
    model = Product
    context_object_name = 'product'

# Sistema de gerenciamento
class ProductListView(ListView):
    model = Product
    context_object_name = 'products'
    template_name = 'products/list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductListView, self).dispatch(request, *args, **kwargs)

class ProductCreateView(CreateView):
    model = Product
    form_class = ProductForm
    template_name = 'products/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Produto cadastrado com sucesso.')
        return reverse_lazy('products:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(ProductCreateView, self).form_invalid(form)

    @method_decorator(permission_required('core.add_product', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(ProductCreateView, self).dispatch(request, *args, **kwargs)

class ProductUpdateView(UpdateView):
    model = Product
    form_class = ProductForm
    template_name = 'products/form.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Produto alterado com sucesso.')
        return reverse_lazy('products:list')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, u'O formulário está inválido.')
        return super(ProductUpdateView, self).form_invalid(form)

    @method_decorator(permission_required('core.change_product', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(ProductUpdateView, self).dispatch(request, *args, **kwargs)

class ProductDetailView(DetailView):
    model = Product
    context_object_name = 'product'
    template_name = 'base/detail.html'

    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ProductDetailView, self).dispatch(request, *args, **kwargs)

class ProductDeleteView(DeleteView):
    model = Product
    context_object_name = 'product'
    template_name = 'base/delete.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, u'Produto excluído.')
        return reverse_lazy('products:list')

    def get_context_data(self, **kwargs):
        context = super(ProductDeleteView, self).get_context_data(**kwargs)

        field_keys, field_values = [], []
        generic_fields_key_value(self, field_keys, field_values)

        context['field_keys'] = field_keys
        context['field_values'] = field_values
        return context

    @method_decorator(permission_required('core.delete_product', reverse_lazy('base:permission_denied')))
    def dispatch(self, request, *args, **kwargs):
        return super(ProductDeleteView, self).dispatch(request, *args, **kwargs)
