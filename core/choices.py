# -*- coding: utf-8 -*-

# Tipo de assunto na mensagem
MSG_TYPE = (
    ('Comercial', 'Comercial'),
    ('Suporte', 'Suporte'),
    ('Desenvolvimento', 'Desenvolvimento'),
)

# Tipo de posição
CHOICE_POSITION_TYPE = (
    ('1', u'1 anuncio'),
    ('2', u'2 anuncios'),
    ('4', u'4 anuncios'),
)

# Dimensão da venda direta
CHOICE_DIMENSION = (
    ('1/1', '1/1 - Tamanho de 1 anuncio'),
    ('1/2', '1/2 - Tamanho de 2 anuncios'),
    ('1/4', '1/4 - Tamanho de 4 anuncios'),
)

# Até 4 posições
CHOICE_POSITION = (
    (1, 1),(2, 2),(3, 3), (4, 4),
)

# Tipo de cliente
CLIENT_TYPE = (
    ('L', u'Laboratório'),
    ('C', u'Clínica'),
    ('O', u'Consultório'),
    ('F', u'Fornecedor'),
)

# Tipo de parceiro
PARTNER_TYPE = (
    ('T', u'Tecnologia'),
    ('S', u'Saúde'),
)
