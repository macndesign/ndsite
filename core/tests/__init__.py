# TestCases
from core.tests.clients.models import ClientModelTestCase
from core.tests.clients.forms import ClientFormTestCase

# Doctests
from core.tests.clients import doctst as clients_doctest

__test__ = {
    'clients_doctest': clients_doctest,
}