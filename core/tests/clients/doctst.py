# coding: utf-8
"""
=======================
Doctest do model Client
=======================

Definindo imports necessários
-----------------------------

>>> import os
>>> from django.conf import settings
>>> from django.core.files.images import ImageFile
>>> from core.models import Client
>>> from core.forms import ClientForm
>>> from django.contrib.auth.models import User

Load fixtures
-------------

>>> from django.core import management
>>> management.call_command('loaddata', 'core/fixtures/client_testdata.json', verbosity=0)
>>> Client.objects.count()
8

Criando um objeto client
------------------------

>>> IMAGE = os.path.join(settings.MEDIA_ROOT, 'clients', 'albalab.png')
>>> IMAGE_FILE = ImageFile(open(IMAGE, 'rb'))
>>> c1 = Client()
>>> c1.name = 'Test Client'
>>> c1.image = IMAGE_FILE
>>> c1.save()
>>> c1.pk
9

Verificando se salvou com norm_name correto
-------------------------------------------

>>> c1.name = 'Test.Client'
>>> c1.save()
>>> c1.norm_name
'testclient'

Criando superuser temporário para fazer alteração no form client
----------------------------------------------------------------

>>> User.objects.create_superuser('mario', 'mario@test.me', 'test@123')
<User: mario>
>>> User.objects.count()
1
>>> user = User.objects.get(pk=1)
>>> user.username
u'mario'
>>> user.is_authenticated()
True

Tentando salvar com localidade inválida no form
-----------------------------------------------

>>> form_data = {'locality': c1.locality}
>>> form = ClientForm(data=form_data)
>>> isinstance(form.instance, Client)
True
>>> form_data['locality'] = 'Fortaleza - TZ'
>>> form = ClientForm(data=form_data)
>>> 'locality' in form.errors.keys()
True

"""