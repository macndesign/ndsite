# coding: utf-8
from core.forms import ClientForm
from core.models import Client
from django.test import TestCase
from django.contrib.auth.models import User

class ClientFormTestCase(TestCase):
    fixtures = ['client_testdata.json']

    def setUp(self):
        """
        Carregando dados iniciais para os testes.
        """
        super(ClientFormTestCase, self).setUp()

        # Criando superuser
        User.objects.create_superuser('mario', 'mario@test.me', 'test@123')
        self.login = self.client.login(username='mario', password='test@123')

        # Instanciando clientes e forms
        self.client_1 = Client.objects.get(pk=1)
        self.client_2 = Client.objects.get(pk=2)
        self.form_1 = ClientForm(instance=self.client_1)
        self.form_2 = ClientForm(instance=self.client_2)

        # Dados de preenchimento do form
        self.form_data = {
            'name': 'Test 1',
            'locality': 'Fortaleza - CE',
            'client_type': 'L',
            'active': True
        }


    def test_init(self):
        """
        Testando as instancias que foram criadas no setUp().
        """

        # Verificando se o usuário está autenticado.
        self.assertTrue(self.login)

        # test form 1
        self.assertTrue(isinstance(self.form_1.instance, Client))
        self.assertEqual(self.form_1.instance.pk, self.client_1.pk)

        # test form 2
        self.assertTrue(isinstance(self.form_2.instance, Client))
        self.assertEqual(self.form_2.instance.pk, self.client_2.pk)


    def test_localidade_invalida(self):
        """
        Testando os casos em que o campo locality é
        preenchido incorretamente.
        """

        # Estado brasileiro válido
        self.form_data['locality'] = 'Fortaleza - TZ'
        form = ClientForm(data=self.form_data)
        self.assertItemsEqual(['image', 'locality'], form.errors)
        self.assertIn(
            u'Insira um estado brasileiro válido. Ex: SP',
            unicode(form.errors.get('locality', None))
        )

        # Inserir 1 separador
        self.form_data['locality'] = 'Fortaleza CE'
        form = ClientForm(data=self.form_data)
        self.assertItemsEqual(['image', 'locality'], form.errors)
        self.assertIn(
            u'Insira um separador entre a cidade e o estado como: Ex: . - , / |',
            unicode(form.errors.get('locality', None))
        )

        # Inserir apenas 1 separador
        self.form_data['locality'] = 'Fortaleza ,. CE'
        form = ClientForm(data=self.form_data)
        self.assertItemsEqual(['image', 'locality'], form.errors)
        self.assertIn(
            u'Insira apenas 1 separador entre a cidade e o estado como: Ex: -',
            unicode(form.errors.get('locality', None))
        )


    def test_localidade_valida(self):
        """
        Testando se o campo localidade não gera erro quando está válido.
        """

        # Instancia o form e retira o campo imagem da validação
        form = ClientForm(data=self.form_data)
        form.fields['image'].required = False

        # Verifica se o campo localidade não contém erros
        self.assertRaises(KeyError, form.errors.get('locality', None))

        # Verifica se o form está válido
        self.assertItemsEqual([], form.errors.keys())
        self.assertTrue(form.is_valid())


    def test_localidade_formatada(self):
        """
        Testando se a formatação automática feita no save() do form
        está funcionando.
        """

        # Primeiro caso: "rio de janeiro,rj" para "Rio de Janeiro - RJ"
        self.form_data['locality'] = 'rio de janeiro,rj'
        form = ClientForm(data=self.form_data)
        form.fields['image'].required = False
        form.save(commit=False)
        self.assertEqual(form.instance.locality, 'Rio de Janeiro - RJ')

        # Segundo caso: "rio de Janeiro.rj" para "Rio de Janeiro - RJ"
        self.form_data['locality'] = 'rio de Janeiro.rj'
        form = ClientForm(data=self.form_data)
        form.fields['image'].required = False
        form.save(commit=False)
        self.assertEqual(form.instance.locality, 'Rio de Janeiro - RJ')

        # Terceiro caso: "rio de Janeiro | rj" para "Rio de Janeiro - RJ"
        self.form_data['locality'] = 'rio de Janeiro | rj'
        form = ClientForm(data=self.form_data)
        form.fields['image'].required = False
        form.save(commit=False)
        self.assertEqual(form.instance.locality, 'Rio de Janeiro - RJ')
