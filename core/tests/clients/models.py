# coding: utf-8
from core.models import Client
from django.test import TestCase

class ClientModelTestCase(TestCase):
    fixtures = ['client_testdata.json']

    def setUp(self):
        """
        Carregando dados iniciais para os testes.
        """

        super(ClientModelTestCase, self).setUp()
        self.client_1 = Client.objects.get(pk=1)
        self.client_2 = Client.objects.get(pk=2)


    def test_unicode(self):
        """
        Verificando se o unicode do objeto corresponde ao name.
        """

        self.assertEqual(self.client_1.__unicode__(), self.client_1.name)


    def test_salvar_name_normalizado(self):
        """
        Verificando se o save está salvando o nome normalizado.
        Regra feita com a seguinte regra no save():

        >>> from core.models import Client
        >>> c1 = Client.objects.get(pk=1)
        >>> c1.name = 'Test.Client'
        >>> c1.norm_name = ''.join(client.name.split('.')).lower()
        >>> print c1.norm_name
        ... 'testclient'
        """

        c1 = self.client_1
        c1.name = 'Test.Client'
        c1.save()
        self.assertEqual(c1.norm_name, 'testclient')
