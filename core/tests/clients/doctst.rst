=======================
Doctest do model Client
=======================

Definindo imports necessários
-----------------------------

    >>> import os
    >>> from django.conf import settings
    >>> from django.core.files.images import ImageFile
    >>> from core.models import Client

Load fixtures
-------------

    >>> from django.core import management
    >>> management.call_command("loaddata", "core/fixtures/client_testdata.json", verbosity=0)
    >>> Client.objects.count()
    8

Criando um objeto client
------------------------

    >>> IMAGE = os.path.join(settings.MEDIA_ROOT, 'clients', 'albalab.png')
    >>> IMAGE_FILE = ImageFile(open(IMAGE, 'rb'))
    >>> c1 = Client()
    >>> c1.name = 'Test Client'
    >>> c1.image = IMAGE_FILE
    >>> c1.save()
    >>> print c1.pk
    9
