# -*- coding: utf-8 -*-
import datetime
from core.validators import format_locality, validate_locality
from django import forms
from django.contrib.localflavor.br.forms import BRPhoneNumberField, BRStateChoiceField
from core.choices import MSG_TYPE, CHOICE_DIMENSION, CHOICE_POSITION, CLIENT_TYPE, PARTNER_TYPE
from core.models import WordNews, MainSlider, DirectSale, Client, Partner, Product, About, ShortNews, Distributor

class WordNewsForm(forms.ModelForm):
    u"""
    Formulário do letreiro
    """

    # Provedor
    provider = forms.CharField(
        label=u'Provedor',
        widget=forms.TextInput(attrs={'style': 'width: 400px'}),
        help_text=u'Quem é o provedor da notícia',
    )

    # Notícia
    words = forms.CharField(
        label=u'Notícia',
        widget=forms.Textarea(attrs={'style': 'width:400px; height:57px'}),
        help_text=u'Notícia principal do letreiro',
    )

    # Link
    link = forms.URLField(
        label=u'Link',
        widget=forms.TextInput(attrs={'style': 'width: 400px'}),
        help_text=u'Fonte da notícia',
    )

    class Meta:
        model = WordNews

class MainSliderForm(forms.ModelForm):
    u"""
    Formulário para administrar o Slider
    """

    # Imagem
    image = forms.ImageField(
        label=u'Imagem',
        help_text=u'(Obrigatório) As imagens tem que ter uma largura de 940px e altura de 270px',
    )

    # Descrição da imagem
    caption = forms.CharField(
        label=u'Descrição',
        widget=forms.TextInput(attrs={'style': 'width: 400px'}),
        help_text=u'Descrição da imagem',
        required=False,
    )

    # Descrição que suporta tags html
    html_caption = forms.CharField(
        label=u'Descrição em html',
        widget=forms.Textarea(attrs={'style': 'width:400px; height:57px'}),
        help_text=u'Descrição da imagem com suporte a tags html',
        required=False,
    )

    # URL ao clicar na imagem
    url = forms.URLField(
        label=u'Link da imagem',
        widget=forms.TextInput(attrs={'style': 'width: 400px'}),
        help_text=u'Link vinculado a imagem',
        required=False,
    )

    class Meta:
        model = MainSlider


class DirectSaleForm(forms.ModelForm):
    u"""
    Formulário para administrar o Slider
    """
    
    # Posição do banner
    position = forms.ChoiceField(
        label=u'Posição',
        choices=CHOICE_POSITION,
        help_text=u'Posição do banner no site.',
    )

    # Dimensão do banner
    dimension = forms.ChoiceField(
        label=u'Dimensão',
        choices=CHOICE_DIMENSION,
        help_text=u'Dimensões: 1/1 (940x90); 1/2 (1: 580x90 e 2: 340x90); 1/4 (1: 280x90, 2: 280x90, 3: 160x90 e 4: 160x90);',
    )

    # Anunciante
    advertiser = forms.CharField(
        label=u'Anunciante',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Empresa ou pessoa física anunciante do banner.',
    )

    # Imagem
    image = forms.ImageField(
        label=u'Imagem',
        help_text=u'Posições 1 e 2 (280 x 90 pixels) e posições 3 e 4 (160 x 90 pixels).',
    )

    # Link do banner
    link = forms.URLField(
        label=u'URL',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Link para o qual é direcionado ao clicar no banner.',
    )

    # Venda direta ativa
    active = forms.BooleanField(
        label=u'Ativo',
        widget=forms.CheckboxInput,
        help_text=u'Venda direta que permanecerá ativa.',
        required=False,
        # validators=[validate_max_active_sale],
    )

    class Meta:
        model = DirectSale

class DirectSaleCreateForm(DirectSaleForm):
    u"""
    Formulário herdando de DirectSaleForm para Create
    """
    def __init__(self, *args, **kwargs):
        super(DirectSaleCreateForm, self).__init__(*args, **kwargs)
        del self.fields['active']
        self.fields['position'].choices = CHOICE_POSITION[:int(DirectSale.objects.all()[0].position_type)]

        if DirectSale.objects.all()[0].position_type == '1':
            self.fields['dimension'].choices = CHOICE_DIMENSION[:1]
        elif DirectSale.objects.all()[0].position_type == '2':
            self.fields['dimension'].choices = CHOICE_DIMENSION[1:2]
        elif DirectSale.objects.all()[0].position_type == '4':
            self.fields['dimension'].choices = CHOICE_DIMENSION[2:3]
        
    class Meta:
        model = DirectSale


class ClientForm(forms.ModelForm):
    u"""
    Formulário para cadastro de clientes ou fornecedores.
    """

    # Nome
    name = forms.CharField(
        label=u'Nome',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Nome do cliente ou fornecedor.',
    )

    # Localidade
    locality = forms.CharField(
        label=u'Localidade',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Local da sede principal do cliente. Ex: São Paulo - SP',
        required=False,
        validators=[validate_locality],
    )

    # Imagem
    image = forms.ImageField(
        label=u'Imagem',
        help_text=u'Logomarca com dimensão de 99 x 63 pixels.',
    )

    # Link da logo e URL do cliente ou fornecedor
    link = forms.URLField(
        label=u'URL',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Link da logo e URL do cliente ou fornecedor.',
        required=False,
    )

    # Email do cliente ou fornecedor
    email = forms.EmailField(
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Email do cliente ou fornecedor. Ex: cliente@email.com',
        required=False,
    )

    map = forms.CharField(
        label=u'Mapa',
        widget=forms.Textarea(attrs={'style': 'width: 275px; height: 57px'}),
        help_text=u'Link de endereço do cliente no google maps ou outro serviço.',
        required=False,
    )

    phone = BRPhoneNumberField(
        label=u'Fone',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Telefone do cliente em formato 99-9999-9999',
        required=False,
    )

    client_type = forms.ChoiceField(
        label=u'Tipo de cliente',
        choices=CLIENT_TYPE,
        widget=forms.Select(attrs={'style': 'width: 275px'}),
        help_text=u'Escolha um tipo de cliente.',
    )

    active = forms.BooleanField(
        label=u'Ativo',
        help_text=u'O cliente ou fornecedor está visível?',
        required=False,
    )

    def save(self, commit=True):
        instance = super(ClientForm, self).save(commit=False)
        format_locality(self, instance)
        instance.save()

    class Meta:
        model = Client

class DistributorForm(forms.ModelForm):
    u"""
    Formulário para cadastro de clientes ou fornecedores.
    """

    # Nome
    name = forms.CharField(
        label=u'Nome',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Nome do distribuidor.',
    )

    # Endereço
    address = forms.CharField(
        label=u'Endereço',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Endereço do distribuidor',
    )

    # Cidade
    city = forms.CharField(
        label=u'Cidade',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Ciadde do distribuidor',
    )

    # Estado
    state = BRStateChoiceField(
        label=u'Estado',
        widget=forms.Select(attrs={'style': 'width: 275px'}),
        help_text=u'Estado do distribuidor',
    )

    # Imagem
    image = forms.ImageField(
        label=u'Imagem',
        help_text=u'Logomarca com dimensão de 99 x 63 pixels.',
    )

    # Link da logo e URL do cliente ou fornecedor
    link = forms.URLField(
        label=u'URL',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Link da logo e URL do distribuidor.',
        required=False,
    )

    # Email do cliente ou fornecedor
    email = forms.EmailField(
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Email do distribuidor. Ex: distribuidor@email.com',
        required=False,
    )

    map = forms.CharField(
        label=u'Mapa',
        widget=forms.Textarea(attrs={'style': 'width: 275px; height: 57px'}),
        help_text=u'Link de endereço do distribuidor no google maps ou outro serviço.',
        required=False,
    )

    phone = BRPhoneNumberField(
        label=u'Fone',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Telefone do cliente em formato 99-9999-9999',
        required=False,
    )

    active = forms.BooleanField(
        label=u'Ativo',
        help_text=u'O cliente ou fornecedor está visível?',
        required=False,
    )

    class Meta:
        model = Distributor

class ProductForm(forms.ModelForm):
    u"""
    Formulário para cadastro de produtos da ND.
    """

    # Nome
    name = forms.CharField(
        label=u'Nome',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Nome do produto.',
    )

    # Imagem
    image = forms.ImageField(
        label=u'Imagem',
        help_text=u'Logomarca com dimensão de 349 x 163 pixels.',
    )

    # Thumb
    thumb = forms.ImageField(
        label=u'Thumb',
        help_text=u'Logomarca thumb com dimensão de 60 x 60 pixels.',
    )

    # Descrição
    description = forms.CharField(
        label=u'Descrição',
        widget=forms.Textarea,
        help_text=u'Descrição do produto.',
    )

    # Descrição curta
    short_description = forms.CharField(
        label=u'Descrição curta',
        widget=forms.Textarea,
        help_text=u'Descrição do produto.',
    )

    # Distribuidores
    distributors = forms.ModelMultipleChoiceField(
        label=u'Distribuidores',
        queryset=Distributor.objects.is_active(),
        widget=forms.CheckboxSelectMultiple(),
        help_text=u'Você pode selecionar nenhum ou vários distribuidores.',
        required=False,
    )

    class Meta:
        model = Product

class PartnerForm(forms.ModelForm):
    u"""
    Formulário para cadastro de parceiros da ND.
    """

    # Nome
    name = forms.CharField(
        label=u'Nome',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Nome do parceiro.',
    )

    # Localidade
    locality = forms.CharField(
        label=u'Localidade',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Local da sede principal do parceiro. Ex: São Paulo - SP',
        required=False,
        validators=[validate_locality]
    )

    # Imagem
    image = forms.ImageField(
        label=u'Imagem',
        help_text=u'Logomarca com dimensão de 160 x 95 pixels.',
    )

    # Descrição
    description = forms.CharField(
        label=u'Descrição',
        widget=forms.Textarea(attrs={'style': 'width: 275px; height: 57px'}),
        help_text=u'Breve descrição sobre o parceiro.',
    )

    # Link da logo e URL do cliente ou fornecedor
    link = forms.URLField(
        label=u'URL',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Link da logo e URL do parceiro.',
        required=False,
    )

    # Email do cliente ou fornecedor
    email = forms.EmailField(
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Email do parceiro. Ex: cliente@email.com',
        required=False,
    )

    phone = BRPhoneNumberField(
        label=u'Fone',
        widget=forms.TextInput(attrs={'style': 'width: 275px'}),
        help_text=u'Telefone do parceiro em formato 99-9999-9999',
        required=False,
    )

    partner_type = forms.ChoiceField(
        label=u'Tipo de parceiro',
        choices=PARTNER_TYPE,
        widget=forms.Select(attrs={'style': 'width: 275px'}),
        help_text=u'Escolha um tipo de parceiro.',
    )

    def save(self, commit=True):
        instance = super(PartnerForm, self).save(commit=False)
        format_locality(self, instance)
        instance.save()

    class Meta:
        model = Partner

class AboutForm(forms.ModelForm):
    u"""
    Formulário pra falar sobre.
    """

    # Título do sobre
    title = forms.CharField(
        label=u'Título',
        widget=forms.TextInput(attrs={'style': 'width: 400px'}),
        help_text=u'Título curto para o texto.',
    )

    # Conteúdo
    body = forms.CharField(
        label=u'Conteúdo',
        widget=forms.Textarea(attrs={'style': 'width: 400px; height: 300px'}),
        help_text=u'Conteúdo sobre.',
    )

    class Meta:
        model = About

class ShortNewsForm(forms.ModelForm):
    u"""
    Formulário pra notícias curtas.
    """

    # Título da notícia curta
    title = forms.CharField(
        label=u'Título',
        widget=forms.TextInput(attrs={'style': 'width: 400px'}),
        help_text=u'Título para notícia curta.',
    )

    # Autor da notícia
    author = forms.CharField(
        label=u'Autor',
        widget=forms.TextInput(attrs={'style': 'width: 400px'}),
        help_text=u'Autor da notícia curta.',
    )

    # Conteúdo
    body = forms.CharField(
        label=u'Conteúdo',
        widget=forms.Textarea(attrs={'style': 'width: 400px; height: 300px'}),
        help_text=u'Conteúdo da notícia curta.',
    )

    # Data e hora da notícia
    news_date = forms.DateTimeField(
        label=u'Data e hora da notícia',
        widget=forms.DateTimeInput(
            format='%d/%m/%Y %H:%M:%S',
            attrs={'style': 'width: 400px'},
        ),
        initial=datetime.datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
        help_text=u'Data e hora da notícia, este conteúdo pode ser alterado. Ex: 05/06/2011 10:25:00',
        required=False,
    )

    # Visibilidade da nortícia
    active = forms.BooleanField(
        label=u'Ativo',
        help_text=u'A notícia está visível?',
        required=False,
    )

    class Meta:
        model = ShortNews

class ContactForm(forms.Form):
    u"""
    Formulário de contato do site.
    """

    # Nome do destinatário
    name = forms.CharField(
        label=u'Nome',
        help_text=u'Seu nome para contato.',
    )

    # Email para contato
    email = forms.EmailField(
        help_text=u'Seu email para contato. Ex: cliente@email.com',
    )

    # Assunto referente ao setor
    subject = forms.ChoiceField(
        label='Assunto',
        choices=MSG_TYPE,
        help_text=u'Se comunicar com qual setor?',
    )

    # Redigir mensagem
    message = forms.CharField(
        label=u'Mensagem',
        widget=forms.Textarea,
        help_text=u'Escreva sua mensagem.',
    )
