# -*- coding: utf-8 -*-
from django.contrib.admin.models import LogEntry
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import User, Group
from core.forms import ClientForm
from django.contrib import admin
from core.models import WordNews, MainSlider, DirectSale, Client, Product, Partner, About, ShortNews, Distributor

class ClientAdmin(admin.ModelAdmin):
    class Meta:
        form = ClientForm

admin.site.register(Client, ClientAdmin)

class DistributorAdmin(admin.ModelAdmin):
    class Meta:
        form = ClientForm

admin.site.register(Distributor, DistributorAdmin)

class DirectSaleAdmin(admin.ModelAdmin):
    list_display = ('advertiser', 'position', 'dimension', 'position_type','active')

admin.site.register(DirectSale, DirectSaleAdmin)

class UserCustom(User):
    """
    Classe que herda de User apenas para ter a listagem dos grupos
    do usuário, já que o list_display não suporta ManyToMany
    """
    @property
    def groups_str(self):
        """
        Método para imprimir o(s) grupo(s) do usuário
        """
        groups = self.groups.all()
        my_list = []
        for g in groups:
            my_list.append(g.name.capitalize())

        if my_list:
            return u', '.join(my_list)
        elif self.is_superuser:
            return u'Super usuário'
        else:
            return 'Sem grupo'

# Inserindo a função na classe User
User.grupos = UserCustom.groups_str

class UserAdminCustom(UserAdmin):
    """
    Herda de UserAdmin para incluir 'Grupos' no list_display
    """
    list_display = ('username', 'email', 'first_name', 'last_name', 'grupos', 'is_staff')

# Tira a classe User do Admin
admin.site.unregister(User)
# Insere a classe User no Admin com outro manipulador
admin.site.register(User, UserAdminCustom)

class GroupCustom(Group):
    @property
    def user_str(self):
        users = self.user_set.all()
        my_list = []
        for u in users:
            my_list.append(u.username)

        if my_list:
            return u', '.join(my_list)
        else:
            return u'Não há usuários nesse grupo'

Group.usuarios = GroupCustom.user_str

class GroupAdminCustom(GroupAdmin):
    list_display = ('name', 'usuarios')

admin.site.unregister(Group)
admin.site.register(Group, GroupAdminCustom)

# Pega ações recentes dos usuários
class LogEntryAdmin(admin.ModelAdmin):
    search_fields = ['user']
    list_display = ('user','content_type','object_repr',)
    list_filter = ['user',]

admin.site.register(LogEntry , LogEntryAdmin)

admin.site.register(WordNews)
admin.site.register(MainSlider)
admin.site.register(Product)
admin.site.register(Partner)
admin.site.register(About)
admin.site.register(ShortNews)