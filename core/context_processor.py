# -*- coding: utf-8 -*-
from core.models import Partner

def statics_context_processor(request):
    u"""
    Insere objetos ao contexto.
    """

    partners = Partner.objects.all()

    return {
        'partners': partners,
    }

def authentication_info_context_processor(request):
    u"""
    Pega informações adicionais sobre o usuário.
    """
    
    try:
        client_address = request.META['HTTP_X_FORWARDED_FOR']
    except Exception:
        client_address = request.META['REMOTE_ADDR']

    try:
        hostname = request.META['REMOTE_HOST']
    except Exception:
        hostname = u'Sem Hostname'

    return {
        'client_address': client_address,
        'hostname': hostname,
    }
