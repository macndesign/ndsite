# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MainSlider'
        db.create_table('core_mainslider', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('caption', self.gf('django.db.models.fields.CharField')(max_length=120, blank=True)),
            ('html_caption', self.gf('django.db.models.fields.CharField')(max_length=120, blank=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('core', ['MainSlider'])

        # Adding model 'WordNews'
        db.create_table('core_wordnews', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('provider', self.gf('django.db.models.fields.CharField')(max_length=75)),
            ('words', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('link', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('core', ['WordNews'])

        # Adding model 'DirectSale'
        db.create_table('core_directsale', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('position', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('position_type', self.gf('django.db.models.fields.CharField')(max_length=1, blank=True)),
            ('dimension', self.gf('django.db.models.fields.CharField')(max_length=5, blank=True)),
            ('advertiser', self.gf('django.db.models.fields.CharField')(max_length=75)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('link', self.gf('django.db.models.fields.URLField')(unique=True, max_length=200)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('core', ['DirectSale'])

        # Adding model 'Client'
        db.create_table('core_client', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=75)),
            ('locality', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('link', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('map', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=15, blank=True)),
            ('client_type', self.gf('django.db.models.fields.CharField')(max_length='1')),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('norm_name', self.gf('django.db.models.fields.CharField')(max_length=75, blank=True)),
        ))
        db.send_create_signal('core', ['Client'])

        # Adding model 'Distributor'
        db.create_table('core_distributor', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=75)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=75)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=75)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('link', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('map', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=15, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('norm_name', self.gf('django.db.models.fields.CharField')(max_length=75, blank=True)),
        ))
        db.send_create_signal('core', ['Distributor'])

        # Adding model 'Product'
        db.create_table('core_product', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=75)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('link', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('thumb', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('short_description', self.gf('django.db.models.fields.TextField')()),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('core', ['Product'])

        # Adding M2M table for field distributors on 'Product'
        db.create_table('core_product_distributors', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('product', models.ForeignKey(orm['core.product'], null=False)),
            ('distributor', models.ForeignKey(orm['core.distributor'], null=False))
        ))
        db.create_unique('core_product_distributors', ['product_id', 'distributor_id'])

        # Adding model 'Partner'
        db.create_table('core_partner', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=75)),
            ('locality', self.gf('django.db.models.fields.CharField')(max_length=75, blank=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('link', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=15, blank=True)),
            ('partner_type', self.gf('django.db.models.fields.CharField')(max_length='1')),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('core', ['Partner'])

        # Adding model 'About'
        db.create_table('core_about', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('created_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('core', ['About'])

        # Adding model 'ShortNews'
        db.create_table('core_shortnews', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(unique=True, max_length=120)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, blank=True)),
            ('author', self.gf('django.db.models.fields.CharField')(max_length=75)),
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('news_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2012, 10, 5, 0, 0), blank=True)),
            ('created_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('core', ['ShortNews'])


    def backwards(self, orm):
        # Deleting model 'MainSlider'
        db.delete_table('core_mainslider')

        # Deleting model 'WordNews'
        db.delete_table('core_wordnews')

        # Deleting model 'DirectSale'
        db.delete_table('core_directsale')

        # Deleting model 'Client'
        db.delete_table('core_client')

        # Deleting model 'Distributor'
        db.delete_table('core_distributor')

        # Deleting model 'Product'
        db.delete_table('core_product')

        # Removing M2M table for field distributors on 'Product'
        db.delete_table('core_product_distributors')

        # Deleting model 'Partner'
        db.delete_table('core_partner')

        # Deleting model 'About'
        db.delete_table('core_about')

        # Deleting model 'ShortNews'
        db.delete_table('core_shortnews')


    models = {
        'core.about': {
            'Meta': {'ordering': "['created_date']", 'object_name': 'About'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'body': ('django.db.models.fields.TextField', [], {}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'core.client': {
            'Meta': {'ordering': "['norm_name']", 'object_name': 'Client'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'client_type': ('django.db.models.fields.CharField', [], {'max_length': "'1'"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'locality': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'map': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '75'}),
            'norm_name': ('django.db.models.fields.CharField', [], {'max_length': '75', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'})
        },
        'core.directsale': {
            'Meta': {'ordering': "['position']", 'object_name': 'DirectSale'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'advertiser': ('django.db.models.fields.CharField', [], {'max_length': '75'}),
            'dimension': ('django.db.models.fields.CharField', [], {'max_length': '5', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'link': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '200'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'position_type': ('django.db.models.fields.CharField', [], {'max_length': '1', 'blank': 'True'})
        },
        'core.distributor': {
            'Meta': {'ordering': "['norm_name']", 'object_name': 'Distributor'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '75'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'map': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '75'}),
            'norm_name': ('django.db.models.fields.CharField', [], {'max_length': '75', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '75'})
        },
        'core.mainslider': {
            'Meta': {'object_name': 'MainSlider'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'caption': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True'}),
            'html_caption': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'core.partner': {
            'Meta': {'ordering': "['name']", 'object_name': 'Partner'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'locality': ('django.db.models.fields.CharField', [], {'max_length': '75', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '75'}),
            'partner_type': ('django.db.models.fields.CharField', [], {'max_length': "'1'"}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'})
        },
        'core.product': {
            'Meta': {'ordering': "['name']", 'object_name': 'Product'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'distributors': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['core.Distributor']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '75'}),
            'short_description': ('django.db.models.fields.TextField', [], {}),
            'thumb': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        'core.shortnews': {
            'Meta': {'ordering': "['-news_date']", 'object_name': 'ShortNews'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'author': ('django.db.models.fields.CharField', [], {'max_length': '75'}),
            'body': ('django.db.models.fields.TextField', [], {}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'news_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2012, 10, 5, 0, 0)', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        'core.wordnews': {
            'Meta': {'ordering': "['provider']", 'object_name': 'WordNews'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'provider': ('django.db.models.fields.CharField', [], {'max_length': '75'}),
            'words': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['core']