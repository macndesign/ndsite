# -*- coding: utf-8 -*-
import datetime
from django.core.urlresolvers import reverse
from django.db import models
from django.template.defaultfilters import slugify
from core.choices import CHOICE_POSITION_TYPE, CHOICE_DIMENSION, CLIENT_TYPE, PARTNER_TYPE
from core.managers import MainSliderManager, WordNewsManager, DirectSaleManager, ClientManager, ProductManager, PartnerManager, AboutManager, ShortNewsManager, DistributorManager

class MainSlider(models.Model):
    """
    Slider principal
    """
    image = models.ImageField('Imagem', upload_to='main_slider')
    caption = models.CharField(u'Descrição', max_length=120, blank=True)
    html_caption = models.CharField(u'Desc. HTML', max_length=120, blank=True)
    url = models.URLField('Link', blank=True)
    active = models.BooleanField(u'Ativo', default=False)

    objects = MainSliderManager()

    class Meta:
        verbose_name = 'Slider'
        verbose_name_plural = 'Sliders'

    def __unicode__(self):
        return u'%s' % self.image

class WordNews(models.Model):
    """
    Letreiro
    """
    provider = models.CharField('Provedor', max_length=75)
    words = models.CharField('Palavras', max_length=120)
    link = models.URLField('Link')
    active = models.BooleanField(u'Ativo', default=False)

    objects = WordNewsManager()

    class Meta:
        verbose_name = 'Letreiro'
        verbose_name_plural = 'Letreiros'
        ordering = ['provider']

    def __unicode__(self):
        return u'%s' %  self.provider

class DirectSale(models.Model):
    u"""
    Venda direta de produtos da ND na página inicial.
    """
    position = models.PositiveIntegerField(u'Posição')
    position_type = models.CharField(u'Tipo de posição',
        choices=CHOICE_POSITION_TYPE, max_length=1, blank=True)
    dimension = models.CharField(u'Dimensão', max_length=5,
        choices=CHOICE_DIMENSION, blank=True)
    advertiser = models.CharField(u'Anunciante', max_length=75)
    image = models.ImageField(u'Imagem', upload_to='direct_sale')
    link = models.URLField('Link', unique=True)
    active = models.BooleanField(u'Ativo', default=False)

    objects = DirectSaleManager()

    class Meta:
        verbose_name = 'Venda direta'
        verbose_name_plural = 'Vendas direta'
        ordering = ['position']

    def save(self, *args, **kwargs):
        super(DirectSale, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'%s' % self.advertiser

class Client(models.Model):
    u"""
    Clientes da ND subdividido em 4: laboratórios, clínicas, fornecedores e consultórios.
    """
    name = models.CharField(u'Nome', max_length=75)
    locality = models.CharField(u'Localidade', max_length=150, blank=True)
    image = models.ImageField(u'Logo', upload_to='clients')
    link = models.URLField('Link', blank=True)
    email = models.EmailField(u'E-mail', blank=True)
    map = models.TextField(u'Mapa', blank=True)
    phone = models.CharField(u'Fone', max_length=15, blank=True)
    client_type = models.CharField(u'Tipo de cliente', max_length='1', choices=CLIENT_TYPE)
    active = models.BooleanField(u'Ativo', default=False)

    # Campo para ordenação sem "collate"
    norm_name = models.CharField(u'Nome normalizado', max_length=75, blank=True, editable=False)

    objects = ClientManager()

    def save(self, *args, **kwargs):
        u"""
        Salva name normalizado para o norm_name

        Para fazer a migração basta ir no shell e:
        >>> from core.models import Client
        >>> clients = Client.objects.all()
        >>> for client in clients:
        ...    client.norm_name = ''.join(client.name.split('.')).lower()
        ...    client.save()
        """
        self.norm_name = ''.join(self.name.split('.')).lower()
        super(Client, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        ordering = ['norm_name']

    def __unicode__(self):
        return u'%s' % self.name

class Distributor(models.Model):
    u"""
    Distribuidores da ND classificados por regiões.
    """
    name = models.CharField(u'Nome', max_length=75)
    address = models.CharField(u'Endereço', max_length=150, blank=True)
    city = models.CharField(u'Cidade', max_length=75)
    state = models.CharField(u'Estado', max_length=75)
    image = models.ImageField(u'Logo', upload_to='distributors')
    link = models.URLField('Link', blank=True)
    email = models.EmailField(u'E-mail', blank=True)
    map = models.TextField(u'Mapa', blank=True)
    phone = models.CharField(u'Fone', max_length=15, blank=True)
    active = models.BooleanField(u'Ativo', default=False)

    # Campo para ordenação sem "collate"
    norm_name = models.CharField(u'Nome normalizado', max_length=75, blank=True, editable=False)

    objects = DistributorManager()

    def save(self, *args, **kwargs):
        u"""
        Salva name normalizado para o norm_name

        Para fazer a migração basta ir no shell e:
        >>> from core.models import Client
        >>> clients = Client.objects.all()
        >>> for client in clients:
        ...    client.norm_name = ''.join(client.name.split('.')).lower()
        ...    client.save()
        """
        self.norm_name = ''.join(self.name.split('.')).lower()
        super(Distributor, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Distribuidor'
        verbose_name_plural = 'Distribuidores'
        ordering = ['norm_name']

    def __unicode__(self):
        return u'%s - %s' % (self.name, self.state)

class Product(models.Model):
    u"""
    Produtos da ND.
    """
    name = models.CharField(u'Nome', max_length=75)
    slug = models.SlugField(max_length=75, blank=True)
    image = models.ImageField(u'Logo', upload_to='products')
    link = models.URLField(blank=True)
    thumb = models.ImageField(u'Thumb', upload_to='products')
    description = models.TextField(u'Descrição')
    short_description = models.TextField(u'Descrição curta')
    active = models.BooleanField(u'Ativo', default=False)
    distributors = models.ManyToManyField('Distributor', verbose_name='Distribuidores', blank=True, null=True)

    objects = ProductManager()

    class Meta:
        verbose_name = 'Produto'
        verbose_name_plural = 'Produtos'
        ordering = ['name']

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super(Product, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('base:service', kwargs={'slug': self.slug})

    def __unicode__(self):
        return u'%s' % self.name

class Partner(models.Model):
    u"""
    Parceiros da ND, subdividos em tecnologia e saúde.
    """

    name = models.CharField(u'Nome', max_length=75)
    locality = models.CharField(u'Localidade', max_length=75, blank=True)
    image = models.ImageField(u'Logo', upload_to='partner')
    description = models.TextField(u'Descrição')
    link = models.URLField('Link', blank=True)
    email = models.EmailField(u'E-mail', blank=True)
    phone = models.CharField(u'Fone', max_length=15, blank=True)
    partner_type = models.CharField(u'Tipo de parceiro', max_length='1', choices=PARTNER_TYPE)
    active = models.BooleanField(u'Ativo', default=False)

    objects = PartnerManager()

    class Meta:
        verbose_name = 'Parceiro'
        verbose_name_plural = 'Parceiros'
        ordering = ['name']

    def save(self, *args, **kwargs):
        super(Partner, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'%s' % self.name

class About(models.Model):
    u"""
    Sobre a empresa.
    """

    title = models.CharField(u'Título', max_length=120)
    body = models.TextField(u'Conteúdo')
    created_date = models.DateTimeField(u'Data de criação', auto_now_add=True, editable=False)
    active = models.BooleanField(u'Ativo', default=False)

    objects = AboutManager()

    class Meta:
        verbose_name = 'Sobre'
        verbose_name_plural = 'Lista de sobre'
        ordering = ['created_date']

    def __unicode__(self):
        return  u'%s - %s' % (self.title, self.created_date)

class ShortNews(models.Model):
    u"""
    Notícia curta da página inicial.
    """

    title = models.CharField(u'Título', max_length=120, unique=True)
    slug = models.SlugField(u'Slug', editable=False, blank=True)
    author = models.CharField(u'Autor', max_length=75)
    body = models.TextField(u'Conteúdo')
    news_date = models.DateTimeField(u'Data e hora da notícia', default=datetime.datetime.now(), blank=True)
    created_date = models.DateTimeField(u'Data de criação', auto_now_add=True, editable=False)
    active = models.BooleanField(u'Ativo', default=False)

    # Managers. Ex: news = ShortNews.is_active.all()
    objects = ShortNewsManager()

    class Meta:
        verbose_name = u'Notícia curta'
        verbose_name_plural = u'Notícias curtas'
        ordering = ['-news_date']

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super(ShortNews, self).save(*args, **kwargs)

    def __unicode__(self):
        return  u'%s - %s' % (self.title, self.created_date)
