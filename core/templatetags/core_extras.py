# -*- coding: utf-8 -*-
from django import template
from django.utils.encoding import force_unicode

register = template.Library()

@register.filter(name='br_phone_format')
def br_phone_format(value):
    """
    Template filter para converter valor de
    telefone para o formato brasileiro.

    Ex: No topo do template dar um
    {% load core_extras %}

    Aplicar a template filter em '99-9999-9999'
    {% person.phone|br_phone_format %}

    In: 99-9999-9999
    Out: (99) 9999-9999
    """

    split_val = value.split('-')
    return '({0}) {1}-{2}'.format(
        split_val[0],
        split_val[1],
        split_val[2],
    )

@register.filter
def in_group(user, groups):
    """Returns a boolean if the user is in the given group, or comma-separated
    list of groups.

    Usage::

        {% if user|in_group:"Friends" %}
        ...
        {% endif %}

    or::

        {% if user|in_group:"Friends,Enemies" %}
        ...
        {% endif %}

    """
    group_list = force_unicode(groups).split(',')
    return bool(user.groups.filter(name__in=group_list).values('name'))
