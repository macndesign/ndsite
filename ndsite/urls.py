# coding: utf-8
from django.conf.urls import patterns, include, url
from django.conf import settings
from hashlib import sha224
from datetime import date
from django.contrib import admin
from ndsite.auth_custom import ValidatingPasswordChangeForm

admin.autodiscover()

def generate_sha224():
    """
    Gera um hash para ser inserido na url do admin
    """
    today = r'%s%s%s' % (date.year, date.month, date.day)
    return r'%s' % sha224(today).hexdigest()


urlpatterns = patterns('',
    (r'^%s/' % generate_sha224(), include(admin.site.urls)),

    # Base rotes
    (r'^', include('core.urls.base', namespace='base')),
    # Letreiros
    (r'^wordnews/', include('core.urls.wordnews', namespace='wordnews')),
    # Slider da página principal
    (r'^mainslider/', include('core.urls.mainslider', namespace='mainslider')),
    # Venda direta
    (r'^directsale/', include('core.urls.directsale', namespace='directsale')),
    # Clientes
    (r'^clientes/', include('core.urls.clients', namespace='clients')),
    # Distribuidores
    (r'^distribuidores/', include('core.urls.distributor', namespace='distributor')),
    # Sobre
    (r'^about/', include('core.urls.about', namespace='about')),
    # Notícias curtas
    (r'^shortnews/', include('core.urls.shortnews', namespace='shortnews')),
    # Produtos
    (r'^services/', include('core.urls.products', namespace='products')),
    # Parceiros
    (r'^parceiros/', include('core.urls.partners', namespace='partners')),

    # Docs
    (r'^docs/', include('core.urls.docs', namespace='nd-docs')),

    # Avaliar
    url(r'^avaliar/', include('avaliar.urls', namespace='avaliar')),

    # Accounts
    url(r'^accounts/login/$', 'ndsite.auth_custom.route_login', name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'template_name': 'accounts/logout.html'}, name='logout'),
    url(r'^accounts/change-password/$', 'django.contrib.auth.views.password_change', {'template_name': 'accounts/change_password.html', 'password_change_form': ValidatingPasswordChangeForm}, name='change_password'),
    url(r'^accounts/change-password/done/$', 'django.contrib.auth.views.password_change_done', {'template_name': 'accounts/change_password_done.html'}, name='change_password_done'),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^%s/(?P<path>.*)$' % settings.MEDIA_URL.strip('/'),
         'django.views.static.serve',
             {'document_root': settings.MEDIA_ROOT, 'show_indexes': False}),
    )
