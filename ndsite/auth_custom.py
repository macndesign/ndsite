# -*- coding: utf-8 -*-
from datetime import datetime
import string
from django import forms
from django.conf import settings
from django.contrib.auth.forms import PasswordChangeForm, AuthenticationForm
from django.contrib.auth.models import User, AnonymousUser
from django.contrib.auth.views import login
from django.core.mail import get_connection
from django.core.mail.message import EmailMessage
from django.utils.encoding import smart_unicode
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from captcha import displayhtml, submit

class ReCaptchaField(forms.CharField):
    default_error_messages = {
        'captcha_invalid': _(u'Captcha inválido')
    }

    def __init__(self, *args, **kwargs):
        self.widget = ReCaptcha
        self.required = True
        super(ReCaptchaField, self).__init__(*args, **kwargs)

    def clean(self, values):
        super(ReCaptchaField, self).clean(values[1])
        recaptcha_challenge_value = smart_unicode(values[0])
        recaptcha_response_value = smart_unicode(values[1])
        check_captcha = submit(recaptcha_challenge_value,
            recaptcha_response_value, settings.RECAPTCHA_PRIVATE_KEY, {})
        if not check_captcha.is_valid:
            raise forms.util.ValidationError(self.error_messages['captcha_invalid'])
        return values[0]


class ReCaptcha(forms.widgets.Widget):
    recaptcha_challenge_name = 'recaptcha_challenge_field'
    recaptcha_response_name = 'recaptcha_response_field'

    def render(self, name, value, attrs=None):
        return mark_safe(u'%s' % displayhtml(settings.RECAPTCHA_PUBLIC_KEY))

    def value_from_datadict(self, data, files, name):
        return [data.get(self.recaptcha_challenge_name, None),
            data.get(self.recaptcha_response_name, None)]

class ValidatingPasswordChangeForm(PasswordChangeForm):

    recaptcha = ReCaptchaField(label="Sou humano")

    MIN_LENGTH = 6

    def _verify_password(self, password, character_set, var_iterable=None, error_message=str()):
        u"""
        Verifica se o caractere está em determinado conjunto.
        """
        for c in password:
            if c in character_set:
                var_iterable = c

        if not var_iterable:
            raise forms.ValidationError(error_message)

    def clean_new_password1(self):
        password1 = self.cleaned_data.get('new_password1')

        # Tamanho mínimo
        if len(password1) < self.MIN_LENGTH:
            raise forms.ValidationError('A nova senha deve ser de pelo menos %d caracteres.' % self.MIN_LENGTH)

        # Número
        self._verify_password(password=password1, character_set=string.digits,
            error_message=u'A nova senha deve conter pelo menos um dígito.')

        # Letra em caixa baixa
        self._verify_password(password=password1, character_set=string.lowercase,
            error_message='A nova senha deve conter pelo menos uma letra em caixa baixa.')

        # Letra em caixa alta
        self._verify_password(password=password1, character_set=string.uppercase,
            error_message='A nova senha deve conter pelo menos uma letra em caixa alta.')

        # Caractere especial
        self._verify_password(password=password1, character_set=string.punctuation,
            error_message='A nova senha deve conter pelo menos um caractere especial.')

        return password1

class ValidatingAuthenticationForm(AuthenticationForm):

    # O authentication_info_context_processor popula esse campo com o IP no template login.html
    ip = forms.CharField(label='IP', widget=forms.HiddenInput, required=False)
    hostname = forms.CharField(label='Hostname', widget=forms.HiddenInput, required=False)
    counter_captcha = forms.IntegerField(label='Counter', widget=forms.HiddenInput, required=False)
    recaptcha = ReCaptchaField(label='Repetir Imagem')

    COUNTER_CAPTCHA = None
    IP_ADDRESS = None
    HOSTNAME = None

    def clean(self):

        self.IP_ADDRESS = self.cleaned_data['ip']
        self.HOSTNAME = self.cleaned_data['hostname']
        self.COUNTER_CAPTCHA = self.cleaned_data['counter_captcha']

        try:
            username = self.cleaned_data['username']
        except KeyError:
            username = u'Sem usuário'

        try:
            password = self.cleaned_data['password']
        except KeyError:
            password = u'Sem senha'

        try:
            error_access_user = User.objects.get(username=username)
        except User.DoesNotExist:
            class OtherAnonymousUser(AnonymousUser):
                def check_password(self, raw_password):
                    return True
            error_access_user = OtherAnonymousUser()

        # Envia o email se for a terceira tentativa e a senha estiver incorreta
        if not error_access_user.check_password(password) and self.COUNTER_CAPTCHA > 2:

            # Instancia de smtp connection
            connection = get_connection()
            msg = EmailMessage(
                subject=u'[Segurança] Tentativa de login',
                body=u'Tentativa de login.\n',
                from_email=settings.EMAIL_HOST_USER,
                to=[settings.EMAIL_HOST_USER],
                connection=connection
            )

            # Informações extra
            now = datetime.now()
            msg.body += u'%s\n\n' % now.strftime("Data/Hora: %d/%m/%Y %H:%M:%S")
            msg.body += u'Hostname: %s\nIP: %s\n' % (self.HOSTNAME, self.IP_ADDRESS)
            msg.body += u'Usuário: %s\nSenha: %s\n' % (username, password)
            msg.body += u'Quantidade de tentativas: %s\n' % self.COUNTER_CAPTCHA

            if error_access_user:
                msg.body += u'Tentaram acessar a conta do usuário "%s".' % error_access_user.username.upper()
                msg.to.append(error_access_user.email)

            else:
                msg.body += u'A tentativa de acesso não tem um usuário associado.'

            # Enviando email
            msg.send()

        super(ValidatingAuthenticationForm, self).clean()

class AuthForm(AuthenticationForm):
    def clean(self):
        """
        Apenas para aparecer campo captcha quando houver erro no login.
        """
        self.fields.insert(-1, 'recaptcha', ReCaptchaField(label='Repetir Imagem'))
        super(AuthForm, self).clean()

def route_login(request):
    """
    Altera form do login dependendo da quantidade de tentativas.
    """

    try:
        counter_captcha = int(request.COOKIES.get('counter_captcha', 0))
    except KeyError:
        counter_captcha = 0

    # O form ValidatingAuthenticationForm que contém o captcha, fica valendo a partir da segunda tentativa.
    if counter_captcha > 1:
        return login(request, authentication_form=ValidatingAuthenticationForm, template_name='accounts/login.html')

    else:
        return login(request, authentication_form=AuthForm, template_name='accounts/login.html')
