# -*- coding: utf-8 -*-
from django.db import models
from datetime import datetime

# Create your models here.
class WaterBoy(models.Model):
    nome = models.CharField('Nome', max_length=75)
    data_troca = models.DateTimeField('Data da troca', default=datetime.now())
    data_atualizacao = models.DateField(u'Data de atualização', default=datetime.now())
    alterado_por = models.CharField(max_length=45)
    
    def save(self, *args, **kwargs):
        self.data_atualizacao = datetime.now()
        super(WaterBoy, self).save(*args, **kwargs)
    
    class Meta:
        """ Ordena pela data de troca da agua """
        ordering = ('data_troca',)
        
    def __unicode__(self):
        """ Define um nome amigavel ao objeto """
        return self.nome
