# -*- coding: utf-8 -*-
from django.contrib import admin
from water.models import WaterBoy
from datetime import datetime


def trocou(modeladmin, request, queryset):
    queryset.update(
        data_troca=datetime.now(),
        alterado_por=request.user.username
    )


class WaterBoyAdmin(admin.ModelAdmin):
    fields = ('nome','data_troca')
    list_display = ('nome', 'data_troca', 'alterado_por')
    search_fields = ['nome']
    list_filter = ['nome']
    date_hierarchy = 'data_troca'
    actions = [trocou]

    class Media:
        js = ("water/js/default.js",)

trocou.short_description = u'Trocou a água'
admin.site.register(WaterBoy, WaterBoyAdmin)
