"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase

class EquipeTestCase(TestCase):
    def setUp(self):
        from avaliar.models import Equipe
        self.pulse = Equipe.objects.create(nome='pulse')
        self.softlab = Equipe.objects.create(nome='softlab')

    def test_se_equipe_salva(self):
        self.assertEqual(self.pulse.nome, 'pulse')
        self.assertEqual(self.softlab.nome, 'softlab')
