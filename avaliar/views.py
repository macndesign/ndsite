# -*- coding: utf-8 -*-
# Create your views here.
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404, redirect
from avaliar.forms import FiltraPeriodoEquipeForm
from avaliar.models import Avaliado, Avaliacao, Periodo, Equipe, Funcionario
from django.utils.datastructures import MultiValueDictKeyError

@login_required
def relatorio_avaliacao(request):
    avaliacoes = Avaliacao.objects.order_by('periodo')
    equipes = Equipe.objects.order_by('nome')
    periodos = Periodo.objects.order_by('-data_avaliacao')
    funcionarios = Funcionario.objects.order_by('usuario')
    avaliados = Avaliado.objects.order_by('nome')

    form_periodo_equipe = FiltraPeriodoEquipeForm(request.GET or None)

    periodo_ativo = False
    if 'periodo' in form_periodo_equipe.data:
        periodo_ativo = True
        avaliacoes = avaliacoes.filter(periodo__pk=form_periodo_equipe.data['periodo'])
        funcionarios = [
            {'equipe': x.equipe.nome,
             'usuario': x.usuario.username,
             'avaliacoes': x.avaliacao_set.filter(periodo__pk=form_periodo_equipe.data['periodo']),
             'avaliados': x.avaliados.all()}\
            for x in funcionarios
        ]

    try:
        request_get_periodo = request.GET['periodo']
    except MultiValueDictKeyError:
        request_get_periodo = None

    if 'equipe' in form_periodo_equipe.data:
        avaliacoes = avaliacoes.filter(avaliado__equipe__pk=form_periodo_equipe.data['equipe'])

    try:
        request_get_equipe = request.GET['equipe']
    except MultiValueDictKeyError:
        request_get_equipe = None

    if 'avaliado' in form_periodo_equipe.data:
        avaliacoes = avaliacoes.filter(avaliado__pk=form_periodo_equipe.data['avaliado'])

    try:
        request_get_avaliado = request.GET['avaliado']
    except MultiValueDictKeyError:
        request_get_avaliado = None


    if request.user.is_superuser:
        return render(request, 'avaliar/relatorio_avaliacao.html',{
            'avaliacoes': avaliacoes,
            'avaliados': avaliados,
            'periodos': periodos,
            'equipes': equipes,
            'funcionarios': funcionarios,
            'request_get_periodo': request_get_periodo,
            'request_get_equipe': request_get_equipe,
            'request_get_avaliado': request_get_avaliado,
            'periodo_ativo': periodo_ativo,
        })
    else:
        return redirect(reverse('admin:index'))


@login_required
def avaliado_detail(request, pk):
    avaliado = get_object_or_404(Avaliado, pk=pk)
    if request.user.is_superuser:
        return render(request, 'avaliar/avaliado_detail.html', {'avaliado': avaliado})
    else:
        return redirect(reverse('admin:index'))
