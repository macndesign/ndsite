# -*- coding: utf-8 -*-
from django.contrib import admin
from avaliar.models import Avaliacao, Funcionario, Avaliado, Periodo, Equipe
from datetime import date

admin.site.register(Equipe)

class AvaliadoAdmin(admin.ModelAdmin):
    list_display = ('nome', 'equipe')

admin.site.register(Avaliado, AvaliadoAdmin)

class FuncionaroAdmin(admin.ModelAdmin):
    list_display = ('usuario', 'equipe')

admin.site.register(Funcionario, FuncionaroAdmin)

admin.site.register(Periodo)

class AvaliacaoAdmin(admin.ModelAdmin):

    list_display = ('periodo', 'avaliado', 'nota_pessoal', 'nota')
    list_filter = ('periodo',)

    def queryset(self, request):
        qs = super(AvaliacaoAdmin, self).queryset(request)
        qs = qs.filter(funcionario__usuario__pk=request.user.pk)
        qs = qs.filter(periodo__data_avaliacao__gt=date.today())
        return qs


    def get_form(self, request, obj=None, **kwargs):
        form = super(AvaliacaoAdmin, self).get_form(request, obj, **kwargs)

        try:
            avaliador = Funcionario.objects.get(usuario=request.user)
        except Funcionario.DoesNotExist:
            equipe = Equipe.objects.create(nome='nd')
            avaliado = Avaliado.objects.create(equipe=equipe, nome='nd')
            avaliador = Funcionario(equipe=equipe, usuario=request.user)
            avaliador.save()
            avaliador.avaliados.add(avaliado)
        
        form.base_fields['funcionario'].queryset = Funcionario.objects.filter(usuario=request.user)
        form.base_fields['funcionario'].initial = avaliador.pk
        form.base_fields['avaliado'].queryset = avaliador.avaliados.all()
        periodo_maior = Periodo.objects.filter(data_avaliacao__gt=date.today())
        form.base_fields['periodo'].queryset = periodo_maior
        form.base_fields['periodo'].initial = periodo_maior[0]
        
        return form

    
    class Media:
        css = {
            'all': ('avaliar/css/style.css',)
        }
        js = ('avaliar/js/script.js',)


admin.site.register(Avaliacao, AvaliacaoAdmin)
