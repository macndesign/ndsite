# -*- coding: utf-8 -*-
import datetime
from django.db import models

class Periodo(models.Model):
    data_avaliacao = models.DateField(u'Data da avaliação', help_text=u'Registro único por conjunto de avaliações', default=datetime.date.today())
    data_alteracao = models.DateTimeField(u'Data de alteração', auto_now=True, editable=False)
    data_criacao = models.DateTimeField(u'Data de criação', auto_now_add=True, editable=False)

    class Meta:
        verbose_name = u'Período'
        ordering = ['-data_avaliacao']

    def __unicode__(self):
        return u'%s' % self.data_avaliacao.strftime(u'%B de %Y').capitalize()


class Equipe(models.Model):
    nome = models.CharField(max_length=75, help_text=u'Nome da equipe a qual o funcionário pertence')
    data_alteracao = models.DateTimeField(u'Data de alteração', auto_now=True, editable=False)
    data_criacao = models.DateTimeField(u'Data de criação', auto_now_add=True, editable=False)

    class Meta:
        ordering = ['nome']

    def __unicode__(self):
        return u'%s' % self.nome


class Avaliado(models.Model):
    equipe = models.ForeignKey('Equipe', help_text=u'Equipe a qual o funcionário a ser avaliado pertence')
    nome = models.CharField(max_length=75)
    data_alteracao = models.DateTimeField(u'Data de alteração', auto_now=True, editable=False)
    data_criacao = models.DateTimeField(u'Data de criação', auto_now_add=True, editable=False)

    class Meta:
        ordering = ['nome']

    def __unicode__(self):
        return u'%s' % self.nome


class Funcionario(models.Model):
    equipe = models.ForeignKey('Equipe', help_text=u'Equipe a qual o funcionário pertence')
    usuario = models.OneToOneField('auth.User', related_name='avaliador')
    avaliados = models.ManyToManyField('Avaliado', related_name='avaliado', null=True, blank=True)
    data_alteracao = models.DateTimeField(u'Data de alteração', auto_now=True, editable=False)
    data_criacao = models.DateTimeField(u'Data de criação', auto_now_add=True, editable=False)

    class Meta:
        verbose_name = u'Funcionário'
        ordering = ['usuario']

    def __unicode__(self):
        return u'%s' % self.usuario


class Avaliacao(models.Model):
    periodo = models.ForeignKey('Periodo', help_text=u'Registro único por conjunto de avaliações')
    funcionario = models.ForeignKey('Funcionario', help_text=u'Funcionário que está avaliando')
    avaliado = models.ForeignKey('Avaliado', help_text=u'Funcionário que está sendo avaliado')
    pessoal = models.TextField(null=True, blank=True, help_text=u'Avaliação pessoal')
    nota_pessoal = models.SmallIntegerField('Nota pessoal', null=True, blank=True, help_text=u'Nota de 0 (zero) a 10 (dez)')
    tecnico = models.TextField(null=True, blank=True, help_text=u'Avaliação profissional')
    nota = models.SmallIntegerField(u'Nota técnico', null=True, blank=True, help_text=u'Nota de 0 (zero) a 10 (dez)')
    data_alteracao = models.DateTimeField(u'Data de alteração', auto_now=True, editable=False)
    data_criacao = models.DateTimeField(u'Data de criação', auto_now_add=True, editable=False)

    class Meta:
        verbose_name = u'Avaliação'
        verbose_name_plural = u'Avaliações'
        unique_together = ('periodo', 'funcionario', 'avaliado')
        ordering = ['periodo']

    def __unicode__(self):
        return u'%s' % self.avaliado
