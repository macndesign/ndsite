# -*- coding: utf-8 -*-
from django import template
from django.db.models.aggregates import Avg
from django.shortcuts import get_object_or_404
from avaliar.models import Avaliado

register = template.Library()

@register.filter(name="media_avaliados")
def media_avaliados(value, periodo_id):
    """
    Calcula a média de cada funcionário avaliado

    Uso:
    {% for avaliacao in avaliacoes %}
        {{ avaliacao.avaliado.pk|media_avaliados:avaliacao.periodo.pk }}
    {% endfor %}
    """

    avaliado = get_object_or_404(Avaliado, pk=value)
    media = avaliado.avaliacao_set.filter(periodo__pk=periodo_id).aggregate(notas=Avg('nota'))

    try:
        return "%0.2f" % media['notas']
    except TypeError:
        media['notas'] = None

@register.filter(name="media_pessoal_avaliados")
def media_pessoal_avaliados(value, periodo_id):
    """
    Calcula a média pessoal de cada funcionário avaliado

    Uso:
    {% for avaliacao in avaliacoes %}
        {{ avaliacao.avaliado.pk|media_avaliados:avaliacao.periodo.pk }}
    {% endfor %}
    """

    avaliado = get_object_or_404(Avaliado, pk=value)
    media = avaliado.avaliacao_set.filter(periodo__pk=periodo_id).aggregate(notas=Avg('nota_pessoal'))

    try:
        return "%0.2f" % media['notas']
    except TypeError:
        media['notas'] = None
