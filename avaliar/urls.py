from django.conf.urls import patterns, url

urlpatterns = patterns('avaliar.views',
    url(r'^relatorio-avaliacao/$', 'relatorio_avaliacao', name='relatorio-avaliacao'),
    url(r'^avaliado/(?P<pk>\d+)/$', 'avaliado_detail', name='avaliado_detail'),
)
