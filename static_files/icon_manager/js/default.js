$(function() {

    (function($) {
        $.fn.removeClassRegEx = function(regex)
        {
            var classes = $(this).attr('class');
            if(!classes || !regex) return false;
            var classArray = [];
            classes = classes.split(' ');
            for(var i=0, len=classes.length; i<len; i++)
                if(!classes[i].match(regex)) classArray.push(classes[i]);
            $(this).attr('class', classArray.join(' '));
        };
    })(jQuery);

    var APP = {};

    APP.link = $("a[class^='mc-sprite']");
    APP.escolha = $(".escolha");
    APP.cores = $("#cores");

    APP.link.click(function(e){
        APP.escolha.css({'top': e.pageY, 'left': e.pageX, 'display': 'block'});
        APP.link = $(this);
    });

    APP.escolha.find("select").change(function(){
        APP.link.removeClassRegEx(/^mc-cor/);
        APP.link.addClass($(this).val());
        APP.escolha.hide();
    });

    APP.escolha.find("a.mc-inverso-fechar-32").click(function(){
        APP.escolha.hide();
    });

    // Cria legendas em cada ícone
    var legs = (function(window, $){
        var _list_icons = $("ul.list-icons");

        return{
            doList: function() {
                var i;
                for (i = 0; i < _list_icons.length; i = i + 1) {
                    // Vars
                    var j;
                    var _list_lis = $(_list_icons[i]).find("li");

                    // Iteration
                    var temp = "<ul class='legends'>";
                    for (j = 0; j < _list_lis.length; j = j + 1) {
                        var _title = $(_list_lis[j]).find("a").attr("title");
                        temp += "<li class='legend'>" + _title + "</li>";
                    }
                    temp += "</ul>";

                    $(_list_icons[i]).after(temp);
                }
            }
        }
    }(window, jQuery));
    // Inicializa método
    legs.doList();

});