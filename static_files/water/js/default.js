(function ($) {
    $(document).ready(function ($) {
        // Seleciona o option que indica que o cara trocou a agua
        $('select option:eq(2)').attr('selected', 'selected');
        // Seleciona o primeiro item da grid
        $('.row1:first td :checkbox').click();
        $('.row1:first').addClass('selected');
    });
})(django.jQuery);